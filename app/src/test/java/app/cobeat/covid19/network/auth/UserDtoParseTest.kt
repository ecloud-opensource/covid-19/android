package app.cobeat.covid19.network.auth

import com.squareup.moshi.Moshi
import org.junit.Test

class UserDtoParseTest {

    companion object {
        private const val USER_JSON = "{\n" +
                "\"id\":2,\n" +
                "\"createdAt\":\"2020-04-28T00:04:32.261Z\",\n" +
                "\"updatedAt\":\"2020-04-28T12:52:41.349Z\",\n" +
                "\"isDeleted\":false,\n" +
                "\"firstName\":\"Angie\",\n" +
                "\"lastName\":\"Shanahan\",\n" +
                "\"email\":\"anshanan@gmail.com\",\n" +
                "\"identification\":\"31068960\",\n" +
                "\"phone\":\"+543412271217\",\n" +
                "\"address\":\"Tucumán\",\n" +
                "\"number\":\"2319\",\n" +
                "\"safehouse\":{\n" +
                "\"type\":\"Point\",\n" +
                "\"coordinates\":[\n" +
                "-32.9388783,\n" +
                "-60.65383\n" +
                "]\n" +
                "},\n" +
                "\"questionnaireAnswered\":true,\n" +
                "\"isQuarantined\":true,\n" +
                "\"finishQuarantine\":false,\n" +
                "\"quarantineFrom\":\"2020-04-28T12:52:41.348Z\",\n" +
                "\"quarantineTo\":\"2020-05-12T12:52:41.348Z\",\n" +
                "\"birthdate\":null,\n" +
                "\"gender\":\"F\",\n" +
                "\"avatarUrl\":null,\n" +
                "\"pregnant\":false,\n" +
                "\"onMovement\":false,\n" +
                "\"outOfSafehouse\":false,\n" +
                "\"probability\":null,\n" +
                "\"status\":\"QUARANTINED\",\n" +
                "\"firebaseToken\":\"eIL9gjp8g_c:APA91bE-74I-3hDPIknnMDu8k5wwSvmKzl2LVkNb4rDAhDlvtrD0FymHQwtZWJugaFVx3x4fYpDYMyfda4Q-7Cl8OilaL78C1gzkyZuvufN25hNsUmos9CnLxQ3owpfDpG3lxxLby6yZ\",\n" +
                "\"idCity\":1,\n" +
                "\"idNeighborhood\":1,\n" +
                "\"idCountry\":1,\n" +
                "\"idZone\":null,\n" +
                "\"idLatestPosition\":20,\n" +
                "\"type\":\"user\"\n" +
                "}"
    }

    @Test
    fun parse_UserDto() {
        val adapter = Moshi.Builder().build().adapter(UserDto::class.java)

        print(adapter.fromJson(USER_JSON))
    }
}