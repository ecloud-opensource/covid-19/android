package app.cobeat.covid19.network

import app.cobeat.covid19.network.auth.CodeValidationResponseDto
import com.squareup.moshi.Moshi
import org.junit.Test

class MoshiSerializationAndParsingTest {

    val json =
        "{\"user\":{\"id\":5,\"createdAt\":\"2020-04-03T02:27:33.213Z\",\"updatedAt\":\"2020-04-03T03:11:31.289Z\",\"isDeleted\":false,\"firstName\":\"Usuario\",\"lastName\":\"Prueba\",\"email\":null,\"identification\":\"99999999\",\"phone\":\"+11111100100\",\"address\":null,\"number\":null,\"safehouse\":null,\"isQuarantined\":false,\"finishQuarantine\":false,\"quarantineFrom\":null,\"quarantineTo\":null,\"gender\":\"NE\",\"avatarUrl\":null,\"pregnant\":false,\"onMovement\":false,\"outOfSafehouse\":false,\"probability\":null,\"status\":null,\"idCity\":null,\"idNeighborhood\":null,\"idCountry\":null,\"idZone\":null,\"idLatestPosition\":null,\"type\":\"user\"},\"token\":\"eyJ0b2tlbiI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5leUpwWkNJNk5Td2lZM0psWVhSbFpFRjBJam9pTWpBeU1DMHdOQzB3TTFRd01qb3lOem96TXk0eU1UTmFJaXdpZFhCa1lYUmxaRUYwSWpvaU1qQXlNQzB3TkMwd00xUXdNem94TVRvek1TNHlPRGxhSWl3aWFYTkVaV3hsZEdWa0lqcG1ZV3h6WlN3aVptbHljM1JPWVcxbElqb2lWWE4xWVhKcGJ5SXNJbXhoYzNST1lXMWxJam9pVUhKMVpXSmhJaXdpWlcxaGFXd2lPbTUxYkd3c0ltbGtaVzUwYVdacFkyRjBhVzl1SWpvaU9UazVPVGs1T1RraUxDSndhRzl1WlNJNklpc3hNVEV4TVRFd01ERXdNQ0lzSW1Ga1pISmxjM01pT201MWJHd3NJbTUxYldKbGNpSTZiblZzYkN3aWMyRm1aV2h2ZFhObElqcHVkV3hzTENKcGMxRjFZWEpoYm5ScGJtVmtJanBtWVd4elpTd2labWx1YVhOb1VYVmhjbUZ1ZEdsdVpTSTZabUZzYzJVc0luRjFZWEpoYm5ScGJtVkdjbTl0SWpwdWRXeHNMQ0p4ZFdGeVlXNTBhVzVsVkc4aU9tNTFiR3dzSW1kbGJtUmxjaUk2SWs1Rklpd2lZWFpoZEdGeVZYSnNJanB1ZFd4c0xDSndjbVZuYm1GdWRDSTZabUZzYzJVc0ltOXVUVzkyWlcxbGJuUWlPbVpoYkhObExDSnZkWFJQWmxOaFptVm9iM1Z6WlNJNlptRnNjMlVzSW5CeWIySmhZbWxzYVhSNUlqcHVkV3hzTENKemRHRjBkWE1pT201MWJHd3NJbWxrUTJsMGVTSTZiblZzYkN3aWFXUk9aV2xuYUdKdmNtaHZiMlFpT201MWJHd3NJbWxrUTI5MWJuUnllU0k2Ym5Wc2JDd2lhV1JhYjI1bElqcHVkV3hzTENKcFpFeGhkR1Z6ZEZCdmMybDBhVzl1SWpwdWRXeHNMQ0owZVhCbElqb2lkWE5sY2lKOS5CS1F5TGNKSXZnRHlpQmhnN1BkZm12MmYzOHZzUXEwREg5dnBSaGxGVnI0IiwiZXhwaXJhdGlvbiI6MTYxNzQzMDgyOX0=\"}"

    @Test
    fun testCodeValidationResponseDto_parsing() {
        val adapter = Moshi.Builder().build().adapter(CodeValidationResponseDto::class.java)
        val fromJson = adapter.fromJson(json)
        println(fromJson)
    }

    /**
     * If you have problems with Moshi and a class serialization or deserialization then replace the class in here and run the test.
     * You will get a detailed error explanation.
     */
    @Test
    fun testAdapterGeneration() {
        Moshi.Builder().build().adapter(UpdateUserRequestDto::class.java)
    }

    @Test
    fun moshiSerializesOnlyTheNonNullValues() {
        val adapter = Moshi.Builder().build().adapter(UpdateUserRequestDto::class.java)
        println(adapter.toJson(UpdateUserRequestDto("Benji")))
    }
}