package app.cobeat.covid19.storage

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import app.cobeat.covid19.BuildConfig
import javax.inject.Inject


class SecureStorageImpl @Inject constructor(private val context: Context) : SecureStorage {

    companion object SecureStorageImpl {
        private const val USER_TOKEN_KEY = "userTokenKey"
        private const val COBEAT_SHARED_PREFERENCES_FILE_NAME = "cobeatSharedPreferences"
    }

    private fun getSharedPreferences(): SharedPreferences {
        return EncryptedSharedPreferences.create(
            BuildConfig.APPLICATION_ID + COBEAT_SHARED_PREFERENCES_FILE_NAME,
            MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
            context,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

    override fun storeUserToken(token: String) {
        getSharedPreferences().edit()?.putString(USER_TOKEN_KEY, token)?.apply()
    }

    override fun getUserToken(): String? {
        return getSharedPreferences().getString(USER_TOKEN_KEY, null)
    }

    override fun isRegistered(): Boolean {
        return getSharedPreferences().contains(USER_TOKEN_KEY)
    }
}