package app.cobeat.covid19.storage

interface SecureStorage {

    fun storeUserToken(token: String)
    fun getUserToken(): String?
    fun isRegistered(): Boolean
}