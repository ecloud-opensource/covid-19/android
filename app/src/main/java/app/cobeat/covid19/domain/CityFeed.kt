package app.cobeat.covid19.domain

data class CityFeed(
    val url: String,
    val title: String,
    val image: String,
    val description: String,
    val order: Int
)