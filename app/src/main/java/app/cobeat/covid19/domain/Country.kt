package app.cobeat.covid19.domain

data class Country(
    val id: Int,
    val name: String,
    val cities: List<City>
)