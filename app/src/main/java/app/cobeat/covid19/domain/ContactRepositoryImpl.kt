package app.cobeat.covid19.domain

import app.cobeat.covid19.network.ContactMessageDto
import app.cobeat.covid19.network.ContactService
import app.cobeat.covid19.util.Result
import app.cobeat.covid19.util.Success
import app.cobeat.covid19.util.processSupportedThrowable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ContactRepositoryImpl @Inject constructor(
    private val contactService: ContactService
) : ContactRepository {
    override suspend fun sendContactMessage(contactMessage: ContactMessage): Result<Unit> {
        return withContext(Dispatchers.IO) {

            try {
                contactService.sendContactMessage(
                    transformContactMessageToContactMessageDto(contactMessage)
                )
                Success(Unit)
            } catch (t: Throwable) {
                processSupportedThrowable<Unit>(t)
            }
        }
    }

    private fun transformContactMessageToContactMessageDto(contactMessage: ContactMessage): ContactMessageDto {
        return with(contactMessage) {
            ContactMessageDto(
                name = name,
                lastName = lastName,
                email = email,
                message = message
            )
        }
    }
}