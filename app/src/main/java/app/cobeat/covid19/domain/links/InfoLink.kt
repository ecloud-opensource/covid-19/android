package app.cobeat.covid19.domain.links

data class InfoLink(
    val title: String,
    val url: String,
    val image: String?,
    val description: String,
    val order: Long
)