package app.cobeat.covid19.domain

class Notification(
    val type: String,
    val title: String
) {
    companion object {
        const val HEADER = "HEADER"
        const val FOOTER = "FOOTER"
    }
}