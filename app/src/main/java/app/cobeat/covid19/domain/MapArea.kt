package app.cobeat.covid19.domain

import com.google.android.gms.maps.model.LatLng


data class MapArea(
    val northWest: LatLng,
    val northEast: LatLng,
    val southEast: LatLng,
    val southWest: LatLng
)