package app.cobeat.covid19.domain

import com.google.android.gms.maps.model.LatLng

class Point(
    val type: String,
    val coordinates: LatLng
)