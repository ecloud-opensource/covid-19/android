package app.cobeat.covid19.domain

enum class UserStatusType {
    QUARANTINED,
    ON_RISK,
    EXPOSED,
    WITH_SYMPTOMS,
    HEALTHY
}