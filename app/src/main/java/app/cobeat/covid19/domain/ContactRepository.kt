package app.cobeat.covid19.domain

import app.cobeat.covid19.util.Result

interface ContactRepository {
    suspend fun sendContactMessage(contactMessage: ContactMessage): Result<Unit>
}