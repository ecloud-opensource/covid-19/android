package app.cobeat.covid19.domain

import app.cobeat.covid19.util.Result

interface RankingRepository {

    suspend fun getRanking(id: Int): Result<RankingCity>

    suspend fun getCountry(): Result<RankingCountry>
}