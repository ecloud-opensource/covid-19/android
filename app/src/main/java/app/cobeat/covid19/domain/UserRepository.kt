package app.cobeat.covid19.domain

import app.cobeat.covid19.network.auth.UserDto
import app.cobeat.covid19.util.Result

interface UserRepository {

    suspend fun getUser(): Result<User>
    suspend fun updateUser(user: User): Result<User>
    suspend fun saveSafeHouse(safeHouse: SafeHouse): Result<User>
    fun storeCurrentUser(userDto: UserDto)
    suspend fun getLoggedInUser(): User
    suspend fun checkIfUserIsLoggedInAndRetrieve(): Result<User?>
    suspend fun validatePhone(registration: Registration): Result<String?>
    suspend fun validateCode(
        phone: String,
        code: String
    ): Result<User>
}