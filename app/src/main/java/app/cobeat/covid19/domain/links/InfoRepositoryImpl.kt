package app.cobeat.covid19.domain.links

import app.cobeat.covid19.network.links.InfoLinkDto
import app.cobeat.covid19.network.links.InfoService
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Result
import app.cobeat.covid19.util.Success
import app.cobeat.covid19.util.processSupportedThrowable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class InfoRepositoryImpl @Inject constructor(
    private val infoService: InfoService
) : InfoRepository {

    override suspend fun getInfoLinkList(): Result<List<InfoGroup>> {
        return withContext(Dispatchers.IO) {
            try {
                val infoList = transformInfoLinkDtoToInfoLink(
                    infoService.getInfoLinkList()
                )
                Success(infoList)
            } catch (e: Exception) {
                processSupportedThrowable<List<InfoGroup>>(e)
            }
        }
    }

    private fun transformInfoLinkDtoToInfoLink(infoLinkListDto: InfoLinkDto): List<InfoGroup> {
        return mutableListOf<InfoGroup>().apply {
            add(transformToLinksGroup(infoLinkListDto.country))
            add(transformToLinksGroup(infoLinkListDto.city))
        }
    }

    private fun transformToLinksGroup(place: InfoLinkDto.PlaceDto): InfoGroup {
        return with(place) {
            InfoGroup(
                name = name,
                links = links?.map {
                    InfoLink(
                        title = it.title,
                        url = it.url,
                        image = it.image,
                        description = it.description,
                        order = it.order
                    )
                }
            )
        }
    }
}