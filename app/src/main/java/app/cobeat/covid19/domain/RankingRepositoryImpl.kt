package app.cobeat.covid19.domain

import app.cobeat.covid19.network.RankingCityDto
import app.cobeat.covid19.network.RankingService
import app.cobeat.covid19.network.ranking.CountryResponseDto
import app.cobeat.covid19.util.Result
import app.cobeat.covid19.util.Success
import app.cobeat.covid19.util.processSupportedThrowable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RankingRepositoryImpl @Inject constructor(
    private val rankingService: RankingService
) : RankingRepository {
    override suspend fun getRanking(id: Int): Result<RankingCity> {
        return withContext(Dispatchers.IO) {
            try {
                Success(
                    transformRankingDtoToRanking(
                        rankingService.getCityRanking(id)
                    )
                )

            } catch (t: Throwable) {
                processSupportedThrowable<RankingCity>(t)
            }
        }
    }

    override suspend fun getCountry(): Result<RankingCountry> {
        return withContext(Dispatchers.IO) {
            try {
                val countries = rankingService.getCountry()
                Success(transformCountryDtoToCountry(countries))
            } catch (t: Throwable) {
                processSupportedThrowable<RankingCountry>(t)
            }
        }
    }

    private fun transformRankingDtoToRanking(rankingDto: RankingCityDto): RankingCity {
        return with(rankingDto) {
            RankingCity(
                cityList = rows.map { cityDto ->
                    City(
                        id = cityDto.id,
                        name = cityDto.name,
                        areInfected = cityDto.areInfected,
                        areQuarantined = cityDto.areQuarantined,
                        areOnRisk = cityDto.areOnRisk,
                        areExposed = cityDto.areExposed,
                        areWithSymptoms = cityDto.areWithSymptoms,
                        areHealthy = cityDto.areHealthy,
                        fellows = cityDto.fellows
                    )
                }
            )
        }
    }

    private fun transformCountryDtoToCountry(countryDto: CountryResponseDto): RankingCountry {
        return with(countryDto) {
            RankingCountry(
                countryList = rows.map {
                    Country(
                        id = it.id,
                        name = it.name,
                        cities = it.rows.map { cityDto ->
                            City(
                                id = cityDto.id,
                                name = cityDto.name,
                                areInfected = cityDto.areInfected,
                                areQuarantined = cityDto.areQuarantined,
                                areOnRisk = cityDto.areOnRisk,
                                areExposed = cityDto.areExposed,
                                areWithSymptoms = cityDto.areWithSymptoms,
                                areHealthy = cityDto.areHealthy,
                                fellows = cityDto.fellows
                            )
                        }
                    )
                }
            )
        }
    }
}