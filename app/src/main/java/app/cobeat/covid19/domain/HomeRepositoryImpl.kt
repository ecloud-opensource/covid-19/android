package app.cobeat.covid19.domain

import app.cobeat.covid19.network.CityDto
import app.cobeat.covid19.network.CityFeedDto
import app.cobeat.covid19.network.HomeService
import app.cobeat.covid19.network.InternalException
import app.cobeat.covid19.network.nearby.NearbyRequestDto
import app.cobeat.covid19.network.nearby.NearbyResponseDto
import app.cobeat.covid19.network.nearby.NearbyService
import app.cobeat.covid19.network.nearby.PointDto
import app.cobeat.covid19.util.Result
import app.cobeat.covid19.util.Success
import app.cobeat.covid19.util.processSupportedThrowable
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class HomeRepositoryImpl @Inject constructor(
    private val homeService: HomeService,
    private val nearbyService: NearbyService,
    private val userRepository: UserRepository
) : HomeRepository {

    override suspend fun getCityFeed(): Result<List<CityFeed>> {
        return withContext(Dispatchers.IO) {
            try {
                Success(
                    transformCityFeedDtoToCityFeed(
                        homeService.getCityFeed()
                    )
                )
            } catch (t: Throwable) {
                processSupportedThrowable<List<CityFeed>>(t)
            }
        }
    }

    override suspend fun getNearby(mapArea: MapArea): Result<NearbyInfo> {
        return withContext(Dispatchers.IO) {
            try {
                val transformNearbyResponseToMapArea = transformNearbyResponseToMapArea(
                    nearbyService.getNearby(
                        transformMapAreaToNearbyRequest(mapArea)
                    )
                )
                Success(transformNearbyResponseToMapArea)
            } catch (t: Throwable) {
                processSupportedThrowable<NearbyInfo>(t)
            }
        }
    }

    private fun transformMapAreaToNearbyRequest(mapArea: MapArea): NearbyRequestDto {
        return NearbyRequestDto(
            topLeft = PointDto(coordinates = mapArea.northWest.toList()),
            topRight = PointDto(coordinates = mapArea.northEast.toList()),
            bottomRight = PointDto(coordinates = mapArea.southEast.toList()),
            bottomLeft = PointDto(coordinates = mapArea.southWest.toList())
        )
    }

    private fun transformNearbyResponseToMapArea(nearbyResponse: NearbyResponseDto): NearbyInfo {
        val notifications = nearbyResponse.notifications.map {
            Notification(
                type = it.type,
                title = it.title
            )
        }
        val nearbyUsers = arrayListOf<NearbyUser>()
        nearbyResponse.users.forEach {
            val point = it.latestPosition.position.let { p ->
                if (p.coordinates.size != 2) return@forEach
                Point(
                    p.type,
                    LatLng(p.coordinates[0], p.coordinates[1])
                )
            }
            nearbyUsers.add(
                NearbyUser(
                    isQuarantined = it.isQuarantined,
                    latestPoint = point
                )
            )
        }
        return NearbyInfo(
            notification = notifications,
            users = nearbyUsers
        )
    }

    private fun transformCityFeedDtoToCityFeed(cityFeedDtoList: List<CityFeedDto>) =
        cityFeedDtoList.map {
            CityFeed(
                url = it.url,
                title = it.title,
                image = it.image,
                description = it.description,
                order = it.order
            )
        }

    private fun LatLng.toList(): List<Double> {
        return listOf(this.latitude, this.longitude)
    }

    override suspend fun getCity(): Result<City> {
        return withContext(Dispatchers.IO) {
            try {
                val idCity = userRepository.getLoggedInUser().idCity
                    ?: throw InternalException("El ID de ciudad no puede ser nulo")
                Success(transformCityDtoToCity(homeService.getCity(idCity)))
            } catch (t: Throwable) {
                processSupportedThrowable<City>(t)
            }
        }
    }

    private fun transformCityDtoToCity(cityDto: CityDto): City {
        return with(cityDto) {
            City(
                id,
                name,
                areInfected,
                areQuarantined,
                areOnRisk,
                areExposed,
                areWithSymptoms,
                areHealthy,
                fellows
            )
        }
    }
}