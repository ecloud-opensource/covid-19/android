package app.cobeat.covid19.domain

import android.content.Context
import android.net.Uri
import android.util.Log
import app.cobeat.covid19.R
import app.cobeat.covid19.network.UserService

/**
 * Centralizes all the settings values and their respective default (or fallback) values.
 */
class SettingsManager(
    private val context: Context,
    private val userService: UserService
) {
    companion object {
        const val TAG = "SettingsManager"
    }

    private var termsAndConditionsUrl: Uri? = null

    fun fetchTermsAndConditions() {
        try {
            termsAndConditionsUrl = Uri.parse(userService.getTermsAndConditions().url)
        } catch (e: Exception) {
            Log.e(TAG, "Error retrieving the terms and conditions URL")
        }
    }

    fun getTermsAndConditionsUri(): Uri {
        return termsAndConditionsUrl?.let {
            it
        } ?: Uri.parse(context.getString(R.string.cobeat_web_url))

    }
}