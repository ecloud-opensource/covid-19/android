package app.cobeat.covid19.domain

import app.cobeat.covid19.network.StringPointDto
import app.cobeat.covid19.network.UpdateUserRequestDto
import app.cobeat.covid19.network.auth.UserDto

fun transformUserToUpdateUserRequestDto(user: User): UpdateUserRequestDto {
    with(user) {
        return UpdateUserRequestDto(
            firstName = name,
            lastName = lastName,
            email = email,
            phone = phone,
            identification = dni,
            status = status,
            address = address
        )
    }
}

fun transformUserDtoToUser(userDto: UserDto): User {
    with(userDto) {
        return User(
            name = firstName.orEmpty(),
            lastName = lastName.orEmpty(),
            email = email.orEmpty(),
            phone = phone.orEmpty(),
            address = address.orEmpty(),
            number = number.orEmpty(),
            dni = identification.orEmpty(),
            idCity = idCity,
            status = status
        )
    }
}

fun transformSafeHouseToUpdateUserRequestDto(safeHouse: SafeHouse): UpdateUserRequestDto {
    with(safeHouse) {
        return UpdateUserRequestDto(
            address = address,
            number = number,
            safeHouse = StringPointDto(
                coordinates = listOf(
                    point.latitude.toString(),
                    point.longitude.toString()
                )
            )
        )
    }
}