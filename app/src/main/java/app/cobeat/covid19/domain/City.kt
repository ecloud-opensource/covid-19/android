package app.cobeat.covid19.domain

data class City(
    val id: Int?,
    val name: String?,
    val areInfected: Number?,
    val areQuarantined: Number?,
    val areOnRisk: Number?,
    val areExposed: Number?,
    val areWithSymptoms: Number?,
    val areHealthy: Number?,
    val fellows: Number?
)