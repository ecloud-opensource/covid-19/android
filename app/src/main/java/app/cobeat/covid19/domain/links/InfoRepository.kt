package app.cobeat.covid19.domain.links

import app.cobeat.covid19.util.Result

interface InfoRepository {
    suspend fun getInfoLinkList(): Result<List<InfoGroup>>
}