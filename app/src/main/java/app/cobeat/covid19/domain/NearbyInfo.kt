package app.cobeat.covid19.domain

class NearbyInfo(
    val notification: List<Notification>,
    val users: List<NearbyUser>
)