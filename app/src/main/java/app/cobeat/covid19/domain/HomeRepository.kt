package app.cobeat.covid19.domain

import app.cobeat.covid19.util.Result

interface HomeRepository {
    suspend fun getCityFeed(): Result<List<CityFeed>>
    suspend fun getNearby(mapArea: MapArea): Result<NearbyInfo>
    suspend fun getCity(): Result<City>
}