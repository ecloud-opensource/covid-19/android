package app.cobeat.covid19.domain.links

data class InfoGroup(
    val name: String,
    val links: List<InfoLink>?
)