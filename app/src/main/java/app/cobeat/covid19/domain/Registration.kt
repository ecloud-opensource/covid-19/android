package app.cobeat.covid19.domain

data class Registration(
    val phoneNumber: String
)