package app.cobeat.covid19.domain

data class RankingCity(
    val cityList: List<City>
)

