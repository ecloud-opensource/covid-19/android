package app.cobeat.covid19.domain

import com.google.android.gms.maps.model.LatLng

data class SafeHouse(
    val address: String,
    val number: String,
    val point: LatLng
)