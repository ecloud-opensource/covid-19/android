package app.cobeat.covid19.domain

import android.util.Log
import app.cobeat.covid19.network.UpdateUserRequestDto
import app.cobeat.covid19.network.UserService
import app.cobeat.covid19.network.auth.AuthService
import app.cobeat.covid19.network.auth.CodeValidationRequestDto
import app.cobeat.covid19.network.auth.PhoneValidationRequestDto
import app.cobeat.covid19.network.auth.UserDto
import app.cobeat.covid19.storage.SecureStorage
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Result
import app.cobeat.covid19.util.Success
import app.cobeat.covid19.util.processSupportedThrowable
import com.google.android.gms.tasks.Tasks
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val userService: UserService,
    private val authService: AuthService,
    private val secureStorage: SecureStorage,
    private val settingsManager: SettingsManager
) : UserRepository {

    private var user: User? = null

    override fun storeCurrentUser(userDto: UserDto) {
        this.user = transformUserDtoToUser(userDto)
    }

    @Throws(IOException::class, RuntimeException::class)
    override suspend fun getLoggedInUser(): User {
        return user?.let {
            return it
        } ?: transformUserDtoToUser(userService.getUser())
    }

    override suspend fun checkIfUserIsLoggedInAndRetrieve(): Result<User?> {
        return withContext(Dispatchers.IO) {
            try {
                val isUserAuthenticated = isUserAuthenticated()
                val deferredTask = async {
                    settingsManager.fetchTermsAndConditions()
                }
                val user = if (isUserAuthenticated) {
                    getLoggedInUser()
                } else {
                    null
                }

                deferredTask.await()
                Success(user)
            } catch (e: Exception) {
                Failure<User?>(e)
            }
        }
    }

    override suspend fun getUser(): Result<User> {
        return withContext(Dispatchers.IO) {
            try {
                val userDto = userService.getUser()
                Success(transformUserDtoToUser(userDto).also {
                    user = it
                })
            } catch (t: Throwable) {
                processSupportedThrowable<User>(t)
            }
        }
    }

    override suspend fun updateUser(user: User): Result<User> {
        return updateUser(transformUserToUpdateUserRequestDto(user))
    }

    override suspend fun saveSafeHouse(safeHouse: SafeHouse): Result<User> {
        return updateUser(transformSafeHouseToUpdateUserRequestDto(safeHouse))
    }

    private suspend fun updateUser(updateUserRequestDto: UpdateUserRequestDto): Result<User> {
        return withContext(Dispatchers.IO) {
            try {
                val userDto = userService.updateUser(updateUserRequestDto)
                Success(transformUserDtoToUser(userDto).also {
                    user = it
                })
            } catch (t: Throwable) {
                processSupportedThrowable<User>(t)
            }
        }
    }

    companion object {
        const val TAG = "AuthRepository"
    }

    private fun isUserAuthenticated(): Boolean {
        val userToken = secureStorage.getUserToken()
        return userToken?.isNotEmpty() ?: false
    }

    override suspend fun validatePhone(registration: Registration): Result<String?> {
        val phoneValidationDto = PhoneValidationRequestDto(
            phone = registration.phoneNumber
        )
        return withContext(Dispatchers.IO) {
            try {
                val statusResponse = authService.postPhoneValidation(phoneValidationDto)
                Success(statusResponse.message)
            } catch (t: Throwable) {
                processSupportedThrowable<String?>(t)
            }
        }
    }

    override suspend fun validateCode(phone: String, code: String): Result<User> {
        return withContext(Dispatchers.IO) {
            try {

                val firebaseToken = FirebaseInstanceId.getInstance()
                    .retrieveInstanceId().token

                Log.d(TAG, "Firebase token: $firebaseToken")

                val codeValidationDto = CodeValidationRequestDto(
                    phone = phone,
                    code = code,
                    firebaseToken = firebaseToken
                )

                val codeValidationResponse = authService.postCodeValidation(codeValidationDto)
                secureStorage.storeUserToken(codeValidationResponse.token)

                Log.d(TAG, "Logged in user: ${codeValidationResponse.user}")
                val loggedInUser = transformUserDtoToUser(codeValidationResponse.user)
                user = loggedInUser
                Success(loggedInUser)
            } catch (t: Throwable) {
                processSupportedThrowable<User>(t)
            }
        }
    }

    private fun FirebaseInstanceId.retrieveInstanceId(): InstanceIdResult {
        return Tasks.await(this.instanceId)
    }
}