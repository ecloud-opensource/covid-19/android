package app.cobeat.covid19.domain

data class RankingCountry(
    val countryList: List<Country>
)
