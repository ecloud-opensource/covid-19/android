package app.cobeat.covid19.domain

data class User(
    val name: String?,
    val lastName: String?,
    val email: String?,
    val phone: String?,
    val address: String?,
    val number: String?,
    val dni: String?,
    val idCity: String? = null,
    val status: UserStatusType?
)