package app.cobeat.covid19.domain

class NearbyUser(
    val isQuarantined: Boolean,
    val latestPoint: Point
)