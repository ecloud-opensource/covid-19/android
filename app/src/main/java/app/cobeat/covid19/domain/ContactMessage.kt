package app.cobeat.covid19.domain

data class ContactMessage(
    val name: String,
    val lastName: String,
    val email: String,
    val message: String
)
