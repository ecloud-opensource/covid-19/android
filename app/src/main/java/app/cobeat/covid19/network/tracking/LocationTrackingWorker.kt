package app.cobeat.covid19.network.tracking

import android.content.Context
import android.location.Location
import android.util.Log
import androidx.work.*
import app.cobeat.covid19.BuildConfig
import app.cobeat.covid19.network.PointDto
import app.cobeat.covid19.network.auth.AuthInterceptor
import app.cobeat.covid19.network.processCall
import app.cobeat.covid19.storage.SecureStorageImpl
import app.cobeat.covid19.util.hasBackgroundLocationPermission
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Tasks
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Gets the last known user location and posts it to the service. If the user didn't grant the necessary
 * permissions this worker won't be able to work.
 */
class LocationTrackingWorker(
    private val context: Context,
    workerParams: WorkerParameters
) : Worker(
    context, workerParams
) {
    companion object {
        private const val LOCATION_TRACKING_WORKER_TAG =
            "app.cobeat.covid19.network.tracking.LocationTrackingWorker"
        private const val TAG = "LocationTrackingWorker"

        /**
         * The minimum repeat interval fot the [WorkManager] is 15 minutes.
         */
        private const val REPEAT_INTERVAL_IN_MINUTES = 15

        /**
         * Enqueues the [LocationTrackingWorker]. If the worker is already loaded
         * it won't be loaded again since we are using the [ExistingPeriodicWorkPolicy.KEEP] flag.
         */
        fun startWorker(context: Context) {
            val workRequest =
                PeriodicWorkRequestBuilder<LocationTrackingWorker>(
                    REPEAT_INTERVAL_IN_MINUTES.toLong(),
                    TimeUnit.MINUTES
                )
                    .addTag(LOCATION_TRACKING_WORKER_TAG)
                    .build()

            WorkManager.getInstance(context)
                .enqueueUniquePeriodicWork(
                    LOCATION_TRACKING_WORKER_TAG,
                    ExistingPeriodicWorkPolicy.KEEP,
                    workRequest
                )
        }
    }

    override fun doWork(): Result {
        if (hasBackgroundLocationPermission(context)) {
            val trackingService = buildLocationTrackingService()
            val fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(context)
            try {
                val location = Tasks.await(fusedLocationProviderClient.lastLocation)
                sendCurrentLocation(location, trackingService)
                return Result.success()
            } catch (t: Throwable) {
                Log.e(TAG, "Error on postGeoNewPoint : $t")
            }
        } else {
            Log.e(TAG, "Location permission not granted")
        }
        return Result.failure()
    }

    private fun sendCurrentLocation(
        location: Location,
        trackingService: LocationTrackingService
    ) {
        with(location) {
            val pointRequestDto = PointRequestDto(
                PointDto(
                    coordinates = listOf(latitude, longitude)
                )
            )
            trackingService.postGeoNewPoint(pointRequestDto).processCall()
            Log.d(TAG, "Geo New Point [$latitude, $longitude]")
        }
    }

    private fun buildOkHttpClient(
        authInterceptor: AuthInterceptor
    ): OkHttpClient {
        return OkHttpClient
            .Builder()
            .addInterceptor(authInterceptor)
            .build()
    }

    private fun buildMoshiConverterFactory(): MoshiConverterFactory {
        return MoshiConverterFactory.create(Moshi.Builder().build())
    }

    private fun buildRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(buildMoshiConverterFactory())
            .build()
    }

    private fun buildLocationTrackingService(): LocationTrackingService {
        val authInterceptor = AuthInterceptor(SecureStorageImpl(context))
        val okHttpClient = buildOkHttpClient(authInterceptor)
        val retrofit = buildRetrofit(okHttpClient)
        return retrofit.create(LocationTrackingService::class.java)
    }
}