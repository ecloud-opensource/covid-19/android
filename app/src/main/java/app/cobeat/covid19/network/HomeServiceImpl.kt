package app.cobeat.covid19.network

import javax.inject.Inject

class HomeServiceImpl @Inject constructor(
    private val cobeatApi: CobeatApi
) : HomeService {

    override fun getCityFeed(): List<CityFeedDto> {
        return cobeatApi.getCityFeed().processCall().rows
    }

    override fun getCity(id: String): CityDto {
        return cobeatApi.getCity(id).processCall().city
    }
}