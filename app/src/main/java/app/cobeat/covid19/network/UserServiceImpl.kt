package app.cobeat.covid19.network

import app.cobeat.covid19.network.auth.UserDto
import javax.inject.Inject

class UserServiceImpl @Inject constructor(private val cobeatApi: CobeatApi) : UserService {
    override fun getUser(): UserDto {
        return cobeatApi.getUserMe().processCall()
    }

    override fun updateUser(updateUserRequestDto: UpdateUserRequestDto): UserDto {
        return cobeatApi.putUserMe(updateUserRequestDto).processCall().user
    }

    override fun getTermsAndConditions(): TermsAndConditionsResponseDto {
        return cobeatApi.getTermsAndConditions().processCall()
    }

}