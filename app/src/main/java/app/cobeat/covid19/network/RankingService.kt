package app.cobeat.covid19.network

import app.cobeat.covid19.network.ranking.CountryResponseDto

interface RankingService {

    fun getCityRanking(id: Int): RankingCityDto

    fun getCountry(): CountryResponseDto
}