package app.cobeat.covid19.network.auth

import app.cobeat.covid19.domain.UserStatusType
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserDto(
    @field:Json(name = "id") val id: Int?,
    @field:Json(name = "createdAt") val createdAt: String?,
    @field:Json(name = "updatedAt") val updatedAt: String?,
    @field:Json(name = "isDeleted") val isDeleted: Boolean?,
    @field:Json(name = "firstName") val firstName: String?,
    @field:Json(name = "lastName") val lastName: String?,
    @field:Json(name = "email") val email: String?,
    @field:Json(name = "identification") val identification: String?,
    @field:Json(name = "phone") val phone: String?,
    @field:Json(name = "address") val address: String?,
    @field:Json(name = "number") val number: String?,
    @field:Json(name = "safehouse") val safehouse: SafeHouseDto?,
    @field:Json(name = "isQuarantined") val isQuarantined: Boolean?,
    @field:Json(name = "finishQuarantine") val finishQuarantine: Boolean?,
    @field:Json(name = "quarantineFrom") val quarantineFrom: String?,
    @field:Json(name = "quarantineTo") val quarantineTo: String?,
    @field:Json(name = "gender") val gender: String?,
    @field:Json(name = "avatarUrl") val avatarUrl: String?,
    @field:Json(name = "pregnant") val pregnant: Boolean?,
    @field:Json(name = "onMovement") val onMovement: Boolean?,
    @field:Json(name = "outOfSafehouse") val outOfSafehouse: Boolean?,
    @field:Json(name = "probability") val probability: String?,
    @field:Json(name = "status") val status: UserStatusType?,
    @field:Json(name = "idCity") val idCity: String?,
    @field:Json(name = "idNeighborhood") val idNeighborhood: String?,
    @field:Json(name = "idCountry") val idCountry: String?,
    @field:Json(name = "idZone") val idZone: String?,
    @field:Json(name = "idLatestPosition") val idLatestPosition: String?,
    @field:Json(name = "type") val type: String?
) {
    @JsonClass(generateAdapter = true)
    data class SafeHouseDto(
        @field:Json(name = "type") val type: String? = "Point",
        @field:Json(name = "coordinates") val coordinates: List<Double>
    )
}