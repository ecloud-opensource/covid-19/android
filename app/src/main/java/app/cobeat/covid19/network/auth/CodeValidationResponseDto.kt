package app.cobeat.covid19.network.auth

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CodeValidationResponseDto(
    @field:Json(name = "user") val user: UserDto,
    @field:Json(name = "token") val token: String
)
