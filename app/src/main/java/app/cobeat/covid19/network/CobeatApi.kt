package app.cobeat.covid19.network

import app.cobeat.covid19.network.auth.CodeValidationRequestDto
import app.cobeat.covid19.network.auth.CodeValidationResponseDto
import app.cobeat.covid19.network.auth.PhoneValidationRequestDto
import app.cobeat.covid19.network.auth.UserDto
import app.cobeat.covid19.network.links.InfoLinkDto
import app.cobeat.covid19.network.nearby.NearbyRequestDto
import app.cobeat.covid19.network.nearby.NearbyResponseDto
import app.cobeat.covid19.network.ranking.CountryResponseDto
import retrofit2.Call
import retrofit2.http.*

/**
 * The interface which provides methods to get result of webservices
 */
interface CobeatApi {

    @POST("/user/phone-validation")
    fun userPhoneValidation(
        @Body phoneValidationDto: PhoneValidationRequestDto
    ): Call<ResponseWrapper<Any>>

    @POST("/user/code-validation")
    fun userCodeValidation(
        @Body codeValidationDto: CodeValidationRequestDto
    ): Call<ResponseWrapper<CodeValidationResponseDto>>

    @GET("/neighborhood/ranking")
    fun getRankingForNeighborhoods(
        @Query("page") page: Int?,
        @Query("pageSize") pageSize: Int?,
        @Query("code") code: String?,
        @Query("name") name: String?,
        @Query("email") email: String?,
        @Query("description") description: String?,
        @Query("from") from: String?,
        @Query("to") to: String?,
        @Query("term") term: String?,
        @Query("scope") scope: String?,
        @Query("group") group: String?,
        @Query("order") order: String?,
        @Query("status") status: String?
    ): Call<ResponseWrapper<RankingCityDto>>

    @GET("/country/mobile?scope=cities")
    fun getCountries(): Call<ResponseWrapper<CountryResponseDto>>

    @GET("/city/ranking")
    fun getRankingForCities(@Query("idCountry") id: Int): Call<ResponseWrapper<RankingCityDto>>

    @PUT("/user/me")
    fun putUserMe(@Body updateUserRequestDto: UpdateUserRequestDto): Call<ResponseWrapper<SaveUserResponseDto>>

    @GET("/user/me")
    fun getUserMe(): Call<ResponseWrapper<UserDto>>

    @GET("/city/feed")
    fun getCityFeed(): Call<ResponseWrapper<CityFeedResponseDto>>

    @POST("/geo/nearby")
    fun getNearby(@Body area: NearbyRequestDto): Call<ResponseWrapper<NearbyResponseDto>>

    @GET("/city/links")
    fun getInfoLinks(): Call<ResponseWrapper<InfoLinkDto>>

    @GET("/city/mobile/{id}")
    fun getCity(@Path("id") id: String): Call<ResponseWrapper<CityMobileResponseDto>>

    @FormUrlEncoded
    @POST("/user/contact")
    fun userContact(
        @Field("firstName") firstName: String,
        @Field("lastName") lastName: String,
        @Field("email") email: String,
        @Field("message") message: String
    ): Call<ResponseWrapper<Any>>

    @GET("/setting/terms-and-conditions")
    fun getTermsAndConditions(): Call<ResponseWrapper<TermsAndConditionsResponseDto>>
}