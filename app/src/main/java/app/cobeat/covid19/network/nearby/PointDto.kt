package app.cobeat.covid19.network.nearby

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PointDto(
    @field:Json(name = "type") val type: String = "Point",
    @field:Json(name = "coordinates") val coordinates: List<Double>
)