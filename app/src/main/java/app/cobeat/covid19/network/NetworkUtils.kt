package app.cobeat.covid19.network

import app.cobeat.covid19.util.operationErrorMessage
import org.json.JSONObject
import retrofit2.Call

@Throws(Throwable::class)
fun <T> Call<ResponseWrapper<T>>.processCall(): T {
    val serviceExecution = execute()
    serviceExecution.errorBody()?.let {
        val errorBodyJson = JSONObject(it.string())
        if (errorBodyJson.has(ResponseWrapper.MESSAGE_KEY)) {
            throw ServerException(errorBodyJson.get(ResponseWrapper.MESSAGE_KEY).toString())
        } else {
            throw ServerException(operationErrorMessage)
        }
    } ?: serviceExecution.body()?.let {
        return it.data ?: throw ServerException(it.message)
    } ?: throw ServerException(operationErrorMessage)
}

@Throws(Throwable::class)
fun <T> Call<ResponseWrapper<T>>.processCallStatusResponse(): StatusResponse {
    val body = execute().body()
    return if (body == null || !body.status) {
        throw InternalException(body?.message ?: operationErrorMessage)
    } else {
        StatusResponse(body.message ?: "")
    }
}