package app.cobeat.covid19.network.auth

import app.cobeat.covid19.network.StatusResponse

interface AuthService {

    fun postPhoneValidation(phoneValidationDto: PhoneValidationRequestDto): StatusResponse

    fun postCodeValidation(codeValidationDto: CodeValidationRequestDto): CodeValidationResponseDto
}