package app.cobeat.covid19.network.tracking

import app.cobeat.covid19.network.PointDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PointRequestDto(
    @field:Json(name = "point") val point: PointDto
)