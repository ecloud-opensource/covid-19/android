package app.cobeat.covid19.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CityDto(
    @field:Json(name = "id") val id: Int?,
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "code") val code: String?,
    @field:Json(name = "areInfected") val areInfected: Int?,
    @field:Json(name = "areQuarantined") val areQuarantined: Int?,
    @field:Json(name = "areOnRisk") val areOnRisk: Int?,
    @field:Json(name = "areExposed") val areExposed: Int?,
    @field:Json(name = "areWithSymptoms") val areWithSymptoms: Int?,
    @field:Json(name = "areHealthy") val areHealthy: Int?,
    @field:Json(name = "fellows") val fellows: Int?,
    @field:Json(name = "center") val center: PointDto?,
    @field:Json(name = "neighborhoods") val neighborhoods: List<NeighborhoodDto>?
)