package app.cobeat.covid19.network

import javax.inject.Inject

class ContactServiceImpl @Inject constructor(private val cobeatApi: CobeatApi) : ContactService {

    override fun sendContactMessage(contactMessageDto: ContactMessageDto): StatusResponse {
        return with(contactMessageDto) {
            cobeatApi.userContact(
                name,
                lastName,
                email,
                message
            ).processCallStatusResponse()
        }
    }
}