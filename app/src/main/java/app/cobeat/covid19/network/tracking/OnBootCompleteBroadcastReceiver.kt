package app.cobeat.covid19.network.tracking

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class OnBootCompleteBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        intent.action?.let {
            if (it == "android.intent.action.BOOT_COMPLETED") {
                LocationTrackingWorker.startWorker(context)
            }
        }
    }
}