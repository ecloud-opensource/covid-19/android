package app.cobeat.covid19.network

import app.cobeat.covid19.network.ranking.CountryResponseDto
import java.io.IOException
import javax.inject.Inject

class RankingServiceImpl @Inject constructor(
    private val cobeatApi: CobeatApi
) : RankingService {

    @Throws(RuntimeException::class, IOException::class)
    override fun getCityRanking(id: Int): RankingCityDto {
        return cobeatApi.getRankingForCities(id).processCall()
    }

    @Throws(RuntimeException::class, IOException::class)
    override fun getCountry(): CountryResponseDto {
        return cobeatApi.getCountries().processCall()
    }
}