package app.cobeat.covid19.network.nearby

interface NearbyService {
    fun getNearby(area: NearbyRequestDto): NearbyResponseDto
}