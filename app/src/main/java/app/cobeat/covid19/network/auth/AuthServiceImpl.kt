package app.cobeat.covid19.network.auth

import app.cobeat.covid19.network.CobeatApi
import app.cobeat.covid19.network.StatusResponse
import app.cobeat.covid19.network.processCall
import app.cobeat.covid19.network.processCallStatusResponse
import com.squareup.moshi.JsonDataException
import java.io.IOException
import javax.inject.Inject

class AuthServiceImpl @Inject constructor(
    private val api: CobeatApi
) : AuthService {

    @Throws(IOException::class, JsonDataException::class)

    override fun postPhoneValidation(phoneValidationDto: PhoneValidationRequestDto): StatusResponse {
        return api.userPhoneValidation(phoneValidationDto).processCallStatusResponse()
    }

    @Throws(IOException::class, JsonDataException::class)
    override fun postCodeValidation(codeValidationDto: CodeValidationRequestDto): CodeValidationResponseDto {
        return api.userCodeValidation(codeValidationDto).processCall()
    }
}