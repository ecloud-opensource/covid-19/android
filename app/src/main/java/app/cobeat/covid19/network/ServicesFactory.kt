package app.cobeat.covid19.network

import android.content.Context
import app.cobeat.covid19.BuildConfig
import app.cobeat.covid19.network.auth.AuthInterceptor
import app.cobeat.covid19.storage.SecureStorageImpl
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class ServicesFactory(
    val context: Context
) : ServicesAbstractFactory {

    override fun buildOkHttpClient(
        authInterceptor: AuthInterceptor
    ): OkHttpClient {
        return OkHttpClient
            .Builder()
            .addInterceptor(authInterceptor)
            .build()
    }

    override fun buildMoshiConverterFactory(): MoshiConverterFactory {
        return MoshiConverterFactory.create(Moshi.Builder().build())
    }

    override fun buildRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(buildMoshiConverterFactory())
            .build()
    }

    override fun <T> buildLocationTrackingService(clazz: Class<T>): T {
        val authInterceptor = AuthInterceptor(SecureStorageImpl(context))
        val okHttpClient = buildOkHttpClient(authInterceptor)
        val retrofit = buildRetrofit(okHttpClient)
        return retrofit.create(clazz)
    }
}