package app.cobeat.covid19.network.tracking

import app.cobeat.covid19.network.PointDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewGeoPointResponseDto(
    @field:Json(name = "point") val point: NewPointDto
) {
    @JsonClass(generateAdapter = true)
    data class NewPointDto(
        @field:Json(name = "nearQuarantine") val nearQuarantine: Boolean,
        @field:Json(name = "position") val position: PointDto
    )
}