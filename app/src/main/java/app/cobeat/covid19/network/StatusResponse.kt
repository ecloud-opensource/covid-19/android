package app.cobeat.covid19.network

data class StatusResponse(
    val message: String?
)