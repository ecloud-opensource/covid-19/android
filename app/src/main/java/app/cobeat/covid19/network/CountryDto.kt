package app.cobeat.covid19.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CountryDto(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "cities") val rows: List<CityDto>,
    @field:Json(name = "areInfected") val areInfected: Int,
    @field:Json(name = "fellows") val fellows: Int
)

