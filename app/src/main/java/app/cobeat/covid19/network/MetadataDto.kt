package app.cobeat.covid19.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MetadataDto(
    @field:Json(name = "totalResults") val totalResults: Int,
    @field:Json(name = "currentPage") val currentPage: Int,
    @field:Json(name = "pageSize") val pageSize: Int
)
