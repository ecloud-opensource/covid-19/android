package app.cobeat.covid19.network

import app.cobeat.covid19.network.auth.UserDto
import java.io.IOException

interface UserService {

    fun getUser(): UserDto

    /**
     * @throws [IOException] if the Profile is not successfully saved.
     */
    fun updateUser(updateUserRequestDto: UpdateUserRequestDto): UserDto

    fun getTermsAndConditions(): TermsAndConditionsResponseDto
}