package app.cobeat.covid19.network.links

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InfoLinkDto(
    @field:Json(name = "country") val country: PlaceDto,
    @field:Json(name = "city") val city: PlaceDto
) {
    @JsonClass(generateAdapter = true)
    data class PlaceDto(
        @field:Json(name = "name") val name: String,
        @field:Json(name = "link") val links: List<LinkDto>?
    )

    @JsonClass(generateAdapter = true)
    data class LinkDto(
        @field:Json(name = "id") val name: Long,
        @field:Json(name = "createdAt") val createdAt: String?,
        @field:Json(name = "updatedAt") val updatedAt: String?,
        @field:Json(name = "isDeleted") val isDeleted: Boolean,
        @field:Json(name = "title") val title: String,
        @field:Json(name = "url") val url: String,
        @field:Json(name = "image") val image: String?,
        @field:Json(name = "description") val description: String,
        @field:Json(name = "order") val order: Long
//        @field:Json(name = "idCity") val idCity: Long
//        @field:Json(name = "idCountry") val idCountry: Int? = null
    )

}