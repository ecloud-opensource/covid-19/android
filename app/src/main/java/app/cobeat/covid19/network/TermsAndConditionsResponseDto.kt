package app.cobeat.covid19.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TermsAndConditionsResponseDto(
    @field:Json(name = "url") val url: String
)