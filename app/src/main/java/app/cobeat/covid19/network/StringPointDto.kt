package app.cobeat.covid19.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

// TODO Validate why the server is throwing an error if we send Double values for the coordinates.
// This is only happening in Android, in Postman the request works ok.
@JsonClass(generateAdapter = true)
data class StringPointDto(
    @field:Json(name = "type") val type: String? = "Point",
    @field:Json(name = "coordinates") val coordinates: List<String>
)