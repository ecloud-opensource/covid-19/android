package app.cobeat.covid19.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class CityFeedDto(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "createdAt") val createdAt: String,
    @field:Json(name = "updatedAt") val updatedAt: String,
    @field:Json(name = "isDeleted") val isDeleted: Boolean,
    @field:Json(name = "url") val url: String,
    @field:Json(name = "title") val title: String,
    @field:Json(name = "image") val image: String,
    @field:Json(name = "description") val description: String,
    @field:Json(name = "order") val order: Int,
    @field:Json(name = "idCity") val idCity: Int
)