package app.cobeat.covid19.network

interface ContactService {
    fun sendContactMessage(contactMessageDto: ContactMessageDto): StatusResponse
}