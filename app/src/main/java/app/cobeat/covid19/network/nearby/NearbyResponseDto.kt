package app.cobeat.covid19.network.nearby

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NearbyResponseDto(
    @field:Json(name = "users") val users: List<UserDto>,
    @field:Json(name = "notifications") val notifications: List<NotificationDto>
) {
    @JsonClass(generateAdapter = true)
    data class UserDto(
        @field:Json(name = "isQuarantined") val isQuarantined: Boolean,
        @field:Json(name = "latestPosition") val latestPosition: PositionDto
    )

    @JsonClass(generateAdapter = true)
    data class PositionDto(
        @field:Json(name = "position") val position: PointDto
    )

    @JsonClass(generateAdapter = true)
    data class NotificationDto(
        @field:Json(name = "type") val type: String,
        @field:Json(name = "title") val title: String
    )
}
