package app.cobeat.covid19.network

class ServerException(message: String?) : Exception(message)
