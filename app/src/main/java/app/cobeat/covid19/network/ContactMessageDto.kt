package app.cobeat.covid19.network

import com.squareup.moshi.Json

data class ContactMessageDto(
    @field:Json(name = "firstName") val name: String,
    @field:Json(name = "lastName") val lastName: String,
    @field:Json(name = "email") val email: String,
    @field:Json(name = "message") val message: String
)