package app.cobeat.covid19.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RankingCityDto(
    @field:Json(name = "rows") val rows: List<CityDto>,
    @field:Json(name = "metadata") val metadata: MetadataDto
)