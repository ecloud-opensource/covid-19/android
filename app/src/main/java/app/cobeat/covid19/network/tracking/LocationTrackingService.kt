package app.cobeat.covid19.network.tracking

import app.cobeat.covid19.network.ResponseWrapper
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface LocationTrackingService {

    @POST("/geo/new-point")
    fun postGeoNewPoint(@Body pointRequestDto: PointRequestDto): Call<ResponseWrapper<NewGeoPointResponseDto>>

}