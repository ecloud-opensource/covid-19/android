package app.cobeat.covid19.network.links

interface InfoService {
    fun getInfoLinkList(): InfoLinkDto
}