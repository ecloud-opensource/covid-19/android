package app.cobeat.covid19.network

import app.cobeat.covid19.domain.UserStatusType
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * The update user api will only update the populated fields. Do not use blanks for the missing fields.
 * All the fields from this class should be optional and null initialized.
 */
@JsonClass(generateAdapter = true)
data class UpdateUserRequestDto(
    @field:Json(name = "firstName") val firstName: String? = null,
    @field:Json(name = "lastName") val lastName: String? = null,
    @field:Json(name = "email") val email: String? = null,
    @field:Json(name = "identification") val identification: String? = null,
    @field:Json(name = "phone") val phone: String? = null,
    @field:Json(name = "address") val address: String? = null,
    @field:Json(name = "number") val number: String? = null,
    @field:Json(name = "status") val status: UserStatusType? = null,
    @field:Json(name = "safehouse") val safeHouse: StringPointDto? = null,
    @field:Json(name = "firebaseToken") val firebaseToken: String? = null
)