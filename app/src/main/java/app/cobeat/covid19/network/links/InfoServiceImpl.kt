package app.cobeat.covid19.network.links

import app.cobeat.covid19.network.CobeatApi
import app.cobeat.covid19.network.processCall
import javax.inject.Inject

class InfoServiceImpl @Inject constructor(
    private val cobeatApi: CobeatApi
) : InfoService {

    override fun getInfoLinkList(): InfoLinkDto {
        return cobeatApi.getInfoLinks().processCall()
    }
}