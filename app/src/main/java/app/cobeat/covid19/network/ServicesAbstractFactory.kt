package app.cobeat.covid19.network

import app.cobeat.covid19.network.auth.AuthInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

interface ServicesAbstractFactory {

    fun buildOkHttpClient(authInterceptor: AuthInterceptor): OkHttpClient

    fun buildMoshiConverterFactory(): MoshiConverterFactory

    fun buildRetrofit(okHttpClient: OkHttpClient): Retrofit

    fun <T> buildLocationTrackingService(clazz: Class<T>): T
}