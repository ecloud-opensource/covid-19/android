package app.cobeat.covid19.network.auth

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhoneValidationRequestDto(
    @field:Json(name = "phone") val phone: String
)