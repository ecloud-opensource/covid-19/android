package app.cobeat.covid19.network.auth

import com.squareup.moshi.Json

data class PhoneValidationDto(
    @field:Json(name = "expiration") val expiration: Long = 180000,
    @field:Json(name = "message") val message: String
)