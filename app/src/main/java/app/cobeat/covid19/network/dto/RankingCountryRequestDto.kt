package app.cobeat.covid19.network.dto

data class RankingCountryRequestDto(
    val page: Int? = null,
    val pageSize: Int? = null,
    val code: String? = null,
    val name: String? = null,
    val email: String? = null,
    val description: String? = null,
    val from: String? = null,
    val to: String? = null,
    val term: String? = null,
    val scope: String? = null,
    val group: String? = null,
    val order: String? = null,
    val status: String? = null
)