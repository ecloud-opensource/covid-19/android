package app.cobeat.covid19.network.auth

import okhttp3.Interceptor
import okhttp3.Response
import java.util.*
import javax.inject.Inject

class LanguageInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
            chain.request()
                .newBuilder()
                .addHeader("accept-language", Locale.getDefault().language)
                .build()
        )
    }
}