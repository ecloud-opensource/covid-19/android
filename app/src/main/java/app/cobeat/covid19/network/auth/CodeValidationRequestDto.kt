package app.cobeat.covid19.network.auth

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CodeValidationRequestDto(
    @field:Json(name = "phone") val phone: String,
    @field:Json(name = "code") val code: String,
    @field:Json(name = "firebaseToken") val firebaseToken: String
)