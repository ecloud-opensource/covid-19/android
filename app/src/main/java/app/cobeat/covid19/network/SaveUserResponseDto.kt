package app.cobeat.covid19.network

import app.cobeat.covid19.network.auth.UserDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SaveUserResponseDto(
    @field:Json(name = "user") val user: UserDto
)