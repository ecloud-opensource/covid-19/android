package app.cobeat.covid19.network.auth

import app.cobeat.covid19.storage.SecureStorage
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(private val secureStorage: SecureStorage) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = with(chain.request().newBuilder()) {
            secureStorage.getUserToken()?.let {
                addHeader("x-api-key", it)
            }
            build()
        }
        return chain.proceed(request)
    }
}