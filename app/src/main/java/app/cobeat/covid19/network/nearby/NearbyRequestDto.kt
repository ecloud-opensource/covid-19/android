package app.cobeat.covid19.network.nearby

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class NearbyRequestDto(
    @field:Json(name = "topLeft") val topLeft: PointDto,
    @field:Json(name = "topRight") val topRight: PointDto,
    @field:Json(name = "bottomLeft") val bottomLeft: PointDto,
    @field:Json(name = "bottomRight") val bottomRight: PointDto
)