package app.cobeat.covid19.network.ranking

import app.cobeat.covid19.network.CountryDto
import app.cobeat.covid19.network.MetadataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CountryResponseDto(
    @field:Json(name = "rows") val rows: List<CountryDto>,
    @field:Json(name = "metadata") val metadata: MetadataDto
)