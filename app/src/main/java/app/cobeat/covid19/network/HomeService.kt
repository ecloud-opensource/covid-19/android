package app.cobeat.covid19.network

interface HomeService {

    fun getCityFeed(): List<CityFeedDto>

    fun getCity(id: String): CityDto
}