package app.cobeat.covid19.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NeighborhoodDto(
    @field:Json(name = "name") val name: String,
    @field:Json(name = "areInfected") val areInfected: Int, //TODO check if this should be the number of people in quarantine
    @field:Json(name = "fellows") val fellows: Int,
    @field:Json(name = "id") val id: Int
)