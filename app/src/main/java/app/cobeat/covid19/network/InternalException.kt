package app.cobeat.covid19.network

class InternalException(message: String?) : Exception(message)
