package app.cobeat.covid19.network.nearby

import app.cobeat.covid19.network.CobeatApi
import app.cobeat.covid19.network.processCall
import javax.inject.Inject

class NearbyServiceImpl @Inject constructor(
    private val cobeatApi: CobeatApi
) : NearbyService {

    override fun getNearby(area: NearbyRequestDto): NearbyResponseDto {
        return cobeatApi.getNearby(area).processCall()
    }
}