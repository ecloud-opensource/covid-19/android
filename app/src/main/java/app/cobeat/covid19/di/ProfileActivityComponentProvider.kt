package app.cobeat.covid19.di

interface ProfileActivityComponentProvider {
    fun getProfileActivityComponent(): ProfileActivityComponent
}