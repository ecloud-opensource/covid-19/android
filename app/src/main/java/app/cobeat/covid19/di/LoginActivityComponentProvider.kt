package app.cobeat.covid19.di

interface LoginActivityComponentProvider {
    fun getLoginActivityComponentProvider(): LoginActivityComponent
}