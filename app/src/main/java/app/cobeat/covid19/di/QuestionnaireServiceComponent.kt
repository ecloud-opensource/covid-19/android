package app.cobeat.covid19.di

import app.cobeat.covid19.ui.questionnaire.QuestionnaireActivity
import dagger.Subcomponent

@Subcomponent
interface QuestionnaireServiceComponent {

    @Subcomponent.Builder
    interface Builder {

        fun build(): QuestionnaireServiceComponent
    }

    fun inject(questionnaireActivity: QuestionnaireActivity)
}