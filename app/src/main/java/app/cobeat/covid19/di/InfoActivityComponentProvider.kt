package app.cobeat.covid19.di

interface InfoActivityComponentProvider {
    fun getInfoActivityComponent(): InfoActivityComponent
}