package app.cobeat.covid19.di

import androidx.lifecycle.ViewModel
import app.cobeat.covid19.ui.contact.ContactFragment
import app.cobeat.covid19.ui.contact.ContactNavigationHandler
import app.cobeat.covid19.ui.contact.ContactResourceProvider
import app.cobeat.covid19.ui.contact.ContactViewModel
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.Subcomponent
import dagger.multibindings.IntoMap

@Subcomponent(modules = [ContactActivityViewModelBindModule::class])
interface ContactActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun contactNavigationHandler(contactNavigationHandler: ContactNavigationHandler): Builder

        @BindsInstance
        fun contactResourceProvider(contactResourceProvider: ContactResourceProvider): Builder

        fun build(): ContactActivityComponent
    }

    fun inject(contactFragment: ContactFragment)


}

@Module
abstract class ContactActivityViewModelBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(ContactViewModel::class)
    abstract fun provideContactViewModel(contactViewModel: ContactViewModel): ViewModel
}