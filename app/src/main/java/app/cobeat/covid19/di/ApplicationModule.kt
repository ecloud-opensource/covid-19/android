package app.cobeat.covid19.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import app.cobeat.covid19.BuildConfig
import app.cobeat.covid19.domain.*
import app.cobeat.covid19.domain.links.InfoRepository
import app.cobeat.covid19.domain.links.InfoRepositoryImpl
import app.cobeat.covid19.network.*
import app.cobeat.covid19.network.auth.AuthInterceptor
import app.cobeat.covid19.network.auth.AuthService
import app.cobeat.covid19.network.auth.AuthServiceImpl
import app.cobeat.covid19.network.auth.LanguageInterceptor
import app.cobeat.covid19.network.links.InfoService
import app.cobeat.covid19.network.links.InfoServiceImpl
import app.cobeat.covid19.network.nearby.NearbyService
import app.cobeat.covid19.network.nearby.NearbyServiceImpl
import app.cobeat.covid19.storage.SecureStorage
import app.cobeat.covid19.storage.SecureStorageImpl
import app.cobeat.covid19.ui.ViewModelFactory
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module(includes = [ApplicationBindModule::class, ApplicationSingletonBindModule::class])
class ApplicationModule {

    @Singleton
    @Provides
    fun provideSecureStorage(context: Context): SecureStorage {
        return SecureStorageImpl(context)
    }


    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor() = HttpLoggingInterceptor().also {
        it.level = HttpLoggingInterceptor.Level.BODY
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(
        authInterceptor: AuthInterceptor,
        httpLoggingInterceptor: HttpLoggingInterceptor,
        languageInterceptor: LanguageInterceptor
    ): OkHttpClient {
        return OkHttpClient
            .Builder()
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(authInterceptor)
            .addInterceptor(languageInterceptor)
            .build()
    }

    @Singleton
    @Provides
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .build()
    }

    @Singleton
    @Provides
    fun provideMoshiConverterFactory(moshi: Moshi): MoshiConverterFactory {
        return MoshiConverterFactory.create(moshi)
    }

    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        moshiConverterFactory: MoshiConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(moshiConverterFactory)
            .build()
    }

    @Singleton
    @Provides
    fun provideCobeatApi(retrofit: Retrofit): CobeatApi {
        return retrofit.create(CobeatApi::class.java)
    }

    @Singleton
    @Provides
    fun provideSettingsManager(context: Context, userService: UserService): SettingsManager {
        return SettingsManager(context, userService)
    }
}

@Module
abstract class ApplicationBindModule {
    @Binds
    abstract fun provideViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}

@Module
abstract class ApplicationSingletonBindModule {

    @Singleton
    @Binds
    abstract fun provideUserRepository(userRepositoryImpl: UserRepositoryImpl): UserRepository

    @Singleton
    @Binds
    abstract fun provideContactRepository(contactRepositoryImpl: ContactRepositoryImpl): ContactRepository

    @Singleton
    @Binds
    abstract fun provideRankingRepository(rankingRepositoryImpl: RankingRepositoryImpl): RankingRepository

    @Singleton
    @Binds
    abstract fun provideInfoRepository(infoRepositoryImpl: InfoRepositoryImpl): InfoRepository

    @Singleton
    @Binds
    abstract fun provideHomeRepositoryImpl(homeRepositoryImpl: HomeRepositoryImpl): HomeRepository

    @Singleton
    @Binds
    abstract fun provideAuthService(authWebServiceImpl: AuthServiceImpl): AuthService

    @Singleton
    @Binds
    abstract fun provideUserService(userServiceImpl: UserServiceImpl): UserService

    @Singleton
    @Binds
    abstract fun provideContactService(contactServiceImpl: ContactServiceImpl): ContactService

    @Singleton
    @Binds
    abstract fun provideRankingService(rankingServiceImpl: RankingServiceImpl): RankingService

    @Singleton
    @Binds
    abstract fun provideInfoService(infoServiceImpl: InfoServiceImpl): InfoService

    @Singleton
    @Binds
    abstract fun provideHomeService(homeServiceImpl: HomeServiceImpl): HomeService

    @Singleton
    @Binds
    abstract fun provideNearbyService(nearbyServiceImpl: NearbyServiceImpl): NearbyService
}