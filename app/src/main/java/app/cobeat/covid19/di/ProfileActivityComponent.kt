package app.cobeat.covid19.di

import androidx.lifecycle.ViewModel
import app.cobeat.covid19.ui.profile.ProfileFragment
import app.cobeat.covid19.ui.profile.ProfileNavigationHandler
import app.cobeat.covid19.ui.profile.ProfileResourceProvider
import app.cobeat.covid19.ui.profile.ProfileViewModel
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.Subcomponent
import dagger.multibindings.IntoMap

@Subcomponent(modules = [ProfileActivityViewModelBindModule::class])
interface ProfileActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun profileNavigationHandler(profileNavigationHandler: ProfileNavigationHandler): Builder

        @BindsInstance
        fun profileResourceProvider(profileResourceProvider: ProfileResourceProvider): Builder

        fun build(): ProfileActivityComponent
    }

    fun inject(profileFragment: ProfileFragment)


}

@Module
abstract class ProfileActivityViewModelBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun provideProfileViewModel(profileViewModel: ProfileViewModel): ViewModel
}