package app.cobeat.covid19.di

interface HomeActivityComponentProvider {

    fun getHomeActivityComponentProvider(): HomeActivityComponent
}