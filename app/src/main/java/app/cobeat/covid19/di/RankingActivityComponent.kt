package app.cobeat.covid19.di

import androidx.lifecycle.ViewModel
import app.cobeat.covid19.ui.ranking.RankingFragment
import app.cobeat.covid19.ui.ranking.RankingNavigationHandler
import app.cobeat.covid19.ui.ranking.RankingResourceProvider
import app.cobeat.covid19.ui.ranking.RankingViewModel
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.Subcomponent
import dagger.multibindings.IntoMap

@Subcomponent(modules = [RankingActivityViewModelBindModule::class])
interface RankingActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun rankingNavigationHandler(rankingNavigationHandler: RankingNavigationHandler): Builder

        @BindsInstance
        fun rankingResourceProvider(rankingResourceProvider: RankingResourceProvider): Builder

        fun build(): RankingActivityComponent
    }

    fun inject(rankingFragment: RankingFragment)
}

@Module
abstract class RankingActivityViewModelBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(RankingViewModel::class)
    abstract fun provideRankingViewModel(rankingViewModel: RankingViewModel): ViewModel
}