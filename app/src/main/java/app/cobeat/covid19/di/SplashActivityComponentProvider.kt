package app.cobeat.covid19.di

interface SplashActivityComponentProvider {
    fun getSplashActivityComponentProvider(): SplashActivityComponent
}