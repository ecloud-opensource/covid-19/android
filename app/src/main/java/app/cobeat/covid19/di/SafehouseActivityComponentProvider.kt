package app.cobeat.covid19.di

interface SafehouseActivityComponentProvider {

    fun getSafehouseActivityComponentProvider(): SafeHouseActivityComponent
}