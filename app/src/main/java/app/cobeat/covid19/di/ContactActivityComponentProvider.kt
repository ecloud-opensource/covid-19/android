package app.cobeat.covid19.di

interface ContactActivityComponentProvider {
    fun getContactActivityComponent(): ContactActivityComponent
}