package app.cobeat.covid19.di

import androidx.lifecycle.ViewModel
import app.cobeat.covid19.ui.info.InfoFragment
import app.cobeat.covid19.ui.info.InfoNavigationHandler
import app.cobeat.covid19.ui.info.InfoViewModel
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.Subcomponent
import dagger.multibindings.IntoMap

@Subcomponent(modules = [InfoActivityViewModelBindModule::class])
interface InfoActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun infoNavigationHandler(infoNavigationHandler: InfoNavigationHandler): Builder

        fun build(): InfoActivityComponent
    }

    fun inject(infoFragment: InfoFragment)
}

@Module
abstract class InfoActivityViewModelBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(InfoViewModel::class)
    abstract fun provideInfoViewModel(infoViewModel: InfoViewModel): ViewModel
}