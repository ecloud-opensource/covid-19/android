package app.cobeat.covid19.di

import androidx.lifecycle.ViewModel
import app.cobeat.covid19.ui.splash.SplashActivity
import app.cobeat.covid19.ui.splash.SplashNavigationHandler
import app.cobeat.covid19.ui.splash.SplashViewModel
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.Subcomponent
import dagger.multibindings.IntoMap

@Subcomponent(modules = [SplashActivityViewModelBindModule::class])
interface SplashActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun loginNavigationHandler(splashNavigationHandler: SplashNavigationHandler): Builder

        fun build(): SplashActivityComponent
    }

    fun inject(splashActivity: SplashActivity)
}

@Module
abstract class SplashActivityViewModelBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun provideSplashViewModel(splashViewModel: SplashViewModel): ViewModel
}