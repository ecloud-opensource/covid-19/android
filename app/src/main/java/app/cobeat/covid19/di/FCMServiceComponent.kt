package app.cobeat.covid19.di

import app.cobeat.covid19.services.FCMService
import dagger.Subcomponent

@Subcomponent
interface FCMServiceComponent {

    @Subcomponent.Builder
    interface Builder {

        fun build(): FCMServiceComponent
    }

    fun inject(fcmService: FCMService)
}