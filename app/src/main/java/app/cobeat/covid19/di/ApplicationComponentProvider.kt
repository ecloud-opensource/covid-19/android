package app.cobeat.covid19.di

interface ApplicationComponentProvider {

    fun getApplicationComponent(): ApplicationComponent
}