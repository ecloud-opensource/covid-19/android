package app.cobeat.covid19.di

interface RankingActivityComponentProvider {
    fun getRankingActivityComponent(): RankingActivityComponent
}