package app.cobeat.covid19.di

import androidx.lifecycle.ViewModel
import app.cobeat.covid19.ui.login.*
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.Subcomponent
import dagger.multibindings.IntoMap

@Subcomponent(modules = [LoginActivityViewModelBindModule::class])
interface LoginActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun loginNavigationHandler(loginNavigationHandler: LoginNavigationHandler): Builder

        @BindsInstance
        fun loginResourceProvider(loginResourceProvider: LoginResourceProvider): Builder

        fun build(): LoginActivityComponent
    }

    fun inject(loginFragment: LoginFragment)

    fun inject(activationCodeFragment: ActivationCodeFragment)

    fun inject(loginActivity: LoginActivity)
}

@Module
abstract class LoginActivityViewModelBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(ValidatePhoneViewModel::class)
    abstract fun provideLoginViewModel(loginViewModel: ValidatePhoneViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ActivationCodeViewModel::class)
    abstract fun provideActivationCodeViewModel(activationCodeViewModel: ActivationCodeViewModel): ViewModel
}