package app.cobeat.covid19.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(context: Context): Builder

        fun build(): ApplicationComponent
    }

    fun fcmServiceComponentBuilder(): FCMServiceComponent.Builder

    fun splashActivityComponentBuilder(): SplashActivityComponent.Builder

    fun loginActivityComponentBuilder(): LoginActivityComponent.Builder

    fun homeActivityComponentBuilder(): HomeActivityComponent.Builder

    fun profileActivityComponentBuilder(): ProfileActivityComponent.Builder

    fun contactActivityComponentBuilder(): ContactActivityComponent.Builder

    fun rankingActivityComponentBuilder(): RankingActivityComponent.Builder

    fun infoActivityComponentBuilder(): InfoActivityComponent.Builder

    fun safeHouseActivityComponentBuilder(): SafeHouseActivityComponent.Builder

    fun questionnaireActivityComponentBuilder(): QuestionnaireServiceComponent.Builder
}