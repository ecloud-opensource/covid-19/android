package app.cobeat.covid19.di

import androidx.lifecycle.ViewModel
import app.cobeat.covid19.ui.LocationHandler
import app.cobeat.covid19.ui.MapHandler
import app.cobeat.covid19.ui.PermissionHandler
import app.cobeat.covid19.ui.safehouse.*
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.Subcomponent
import dagger.multibindings.IntoMap

@Subcomponent(modules = [SafeHouseActivityViewModelBindModule::class])
interface SafeHouseActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun safehouseNavigationHandler(safehouseNavigationHandler: SafeHouseNavigationHandler): Builder

        @BindsInstance
        fun locationHandler(locationHandler: LocationHandler): Builder

        @BindsInstance
        fun permissionHandler(permissionHandler: PermissionHandler): Builder

        @BindsInstance
        fun mapHandler(mapHandler: MapHandler): Builder

        @BindsInstance
        fun placesHandler(placesHandler: PlacesHandler): Builder

        @BindsInstance
        fun safeHouseResourceProvider(safeHouseResourceProvider: SafeHouseResourceProvider): Builder

        fun build(): SafeHouseActivityComponent
    }

    fun inject(homeFragment: SafeHouseFragment)

    fun inject(homeFragment: SafeHouseBottomFragment)
}

@Module
abstract class SafeHouseActivityViewModelBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(SafeHouseViewModel::class)
    abstract fun provideSafeHouseViewModel(safeHouseViewModel: SafeHouseViewModel): ViewModel
}