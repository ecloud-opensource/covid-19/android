package app.cobeat.covid19.di

import androidx.lifecycle.ViewModel
import app.cobeat.covid19.ui.LocationHandler
import app.cobeat.covid19.ui.MapHandler
import app.cobeat.covid19.ui.PermissionHandler
import app.cobeat.covid19.ui.home.*
import dagger.Binds
import dagger.BindsInstance
import dagger.Module
import dagger.Subcomponent
import dagger.multibindings.IntoMap

@Subcomponent(modules = [HomeActivityViewModelBindModule::class])
interface HomeActivityComponent {

    @Subcomponent.Builder
    interface Builder {

        @BindsInstance
        fun homeNavigationHandler(homeNavigationHandler: HomeNavigationHandler): Builder

        @BindsInstance
        fun homeResourceProvider(homeResourceProvider: HomeResourceProvider): Builder

        @BindsInstance
        fun locationHandler(locationHandler: LocationHandler): Builder

        @BindsInstance
        fun permissionHandler(permissionHandler: PermissionHandler): Builder

        @BindsInstance
        fun mapHandler(mapHandler: MapHandler): Builder

        fun build(): HomeActivityComponent
    }

    fun inject(homeActivity: HomeActivity)

    fun inject(menuFragment: MenuFragment)

    fun inject(homeFragment: HomeFragment)

    fun inject(homeFragment: HomeBottomFragment)
}

@Module
abstract class HomeActivityViewModelBindModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun provideHomeViewModel(homeViewModel: HomeViewModel): ViewModel
}