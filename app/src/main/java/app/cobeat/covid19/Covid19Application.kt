package app.cobeat.covid19

import android.app.Application
import app.cobeat.covid19.di.ApplicationComponent
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.DaggerApplicationComponent
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump


class Covid19Application : Application(), ApplicationComponentProvider {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent.builder()
            .applicationContext(this)
            .build()

        initCalligraphy()
    }

    override fun getApplicationComponent() = applicationComponent

    private fun initCalligraphy() {
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/GothamRoundedBook.otf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )
    }
}