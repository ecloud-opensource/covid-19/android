package app.cobeat.covid19.ui.login

class LoginResourceProvider(
    val phoneNumberError: String,
    val remainingTimeExpiration: String,
    val sendingActivationCodeErrorMessage: String
)