package app.cobeat.covid19.ui.ranking

interface RankingNavigationHandler {
    fun navigateToRankingFragment()
}