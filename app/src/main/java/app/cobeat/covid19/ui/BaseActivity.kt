package app.cobeat.covid19.ui

import android.content.Context
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import app.cobeat.covid19.R
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import kotlinx.android.synthetic.main.loading_layout.*

/**
 * Contains the generic Activity related functions. Do not store shared values in this class.
 */
abstract class BaseActivity : AppCompatActivity(), LoadingViewHandler {

    override fun attachBaseContext(ctx: Context?) {
        ctx?.let {
            super.attachBaseContext(ViewPumpContextWrapper.wrap(ctx))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getContentViewResource())
    }

    /**
     * Provides the Activity layout to be used.
     */
    protected open fun getContentViewResource() = R.layout.activity_base

    override fun showLoadingView() {
        loading_view.visibility = VISIBLE
    }

    override fun hideLoadingView() {
        loading_view.visibility = GONE
    }
}