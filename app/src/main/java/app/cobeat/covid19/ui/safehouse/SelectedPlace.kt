package app.cobeat.covid19.ui.safehouse

import com.google.android.libraries.places.api.model.Place

data class SelectedPlace(
    val place: Place,
    val streetName: String,
    val streetNumber: String,
    val searchQuery: String
)