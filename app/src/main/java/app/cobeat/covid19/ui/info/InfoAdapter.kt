package app.cobeat.covid19.ui.info

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.cobeat.covid19.R
import app.cobeat.covid19.domain.links.InfoGroup
import app.cobeat.covid19.domain.links.InfoLink
import kotlinx.android.synthetic.main.info_link_group.view.*
import kotlinx.android.synthetic.main.info_link_item.view.*

private const val ITEM_TYPE_GROUP = 0
private const val ITEM_TYPE_LINK = 1
private const val ITEM_TYPE_EMPTY = 2

class InfoAdapter(
    infoGroups: List<InfoGroup>,
    private val onSelected: (InfoLink) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<Any>

    init {
        items = convertInfoListToItems(infoGroups)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_TYPE_LINK -> InfoViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.info_link_item,
                    parent,
                    false
                )
            )
            ITEM_TYPE_GROUP -> HeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.info_link_group,
                    parent,
                    false
                )
            )
            else -> EmptyViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.empty_link_item,
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is InfoLink -> ITEM_TYPE_LINK
            is String -> ITEM_TYPE_GROUP
            else -> ITEM_TYPE_EMPTY
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            ITEM_TYPE_GROUP -> onBindHeader(holder as HeaderViewHolder, items[position] as String)
            ITEM_TYPE_LINK -> onBindItem(holder as InfoViewHolder, items[position] as InfoLink)
        }
    }

    override fun getItemCount() = items.size

    private fun onBindHeader(holder: HeaderViewHolder, title: String) {
        holder.title.text = title
    }

    private fun onBindItem(holder: InfoViewHolder, item: InfoLink) {
        holder.title.text = item.title
        holder.description.text = item.title
        holder.itemView.setOnClickListener { onSelected(item) }
    }

    private fun convertInfoListToItems(infoGroup: List<InfoGroup>): List<Any> {
        val items = mutableListOf<Any>()
        infoGroup.forEach {
            items.add(it.name)
            items.addAll(
                it.links ?: listOf(Unit)
            )
        }
        return items
    }
}

class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val title: TextView = view.titleGroup
}

class InfoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val title: TextView = view.title
    val description: TextView = view.description
}

class EmptyViewHolder(view: View) : RecyclerView.ViewHolder(view)
