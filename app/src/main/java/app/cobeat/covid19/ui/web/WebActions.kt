package app.cobeat.covid19.ui.web

interface WebActions {

    fun canGoBack(): Boolean
    fun goBack()
}