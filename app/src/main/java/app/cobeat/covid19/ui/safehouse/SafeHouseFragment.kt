package app.cobeat.covid19.ui.safehouse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import app.cobeat.covid19.R
import app.cobeat.covid19.di.SafehouseActivityComponentProvider
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.ui.MapHandler
import com.google.android.gms.maps.SupportMapFragment
import javax.inject.Inject


class SafeHouseFragment : BaseFragment() {

    @Inject
    lateinit var safeHouseNavigationHandler: SafeHouseNavigationHandler

    @Inject
    lateinit var mapHandler: MapHandler

    private val safeHouseViewModel: SafeHouseViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = SafeHouseFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_safehouse, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as SafehouseActivityComponentProvider)
            .getSafehouseActivityComponentProvider()
            .inject(this)

        // Synthetic is not smart enough to accept tha the cast will work :)
        (childFragmentManager.findFragmentById(R.id.safehouse_map) as SupportMapFragment)
            .getMapAsync(mapHandler)

        safeHouseViewModel.lastLocation.observe(viewLifecycleOwner, Observer {
            mapHandler.moveCameraToLocation(it)
        })
    }
}
