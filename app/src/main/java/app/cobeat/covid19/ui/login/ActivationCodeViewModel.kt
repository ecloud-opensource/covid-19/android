package app.cobeat.covid19.ui.login

import android.os.CountDownTimer
import androidx.lifecycle.*
import app.cobeat.covid19.domain.UserRepository
import app.cobeat.covid19.ui.Event
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import java.util.*
import javax.inject.Inject

class ActivationCodeViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val loginResourceProvider: LoginResourceProvider
) : ViewModel() {

    companion object {
        const val CODE_EXPIRATION_TIME_SEC: Long = 180000
    }

    private val _errorBannerMessage = MutableLiveData<Event<String>>()
    val errorBannerMessage: LiveData<Event<String>>
        get() = _errorBannerMessage

    private val _resendCodeText = MutableLiveData<Boolean>()
    val resendCodeText: LiveData<Boolean>
        get() = _resendCodeText

    private val _countDownText = MutableLiveData<String>()
    val countDownText: LiveData<String>
        get() = _countDownText

    private val requestActivateCode: MutableLiveData<Pair<String, String>> = MutableLiveData()

    val loggedInUserHasAddress: LiveData<Boolean> =
        Transformations.switchMap(requestActivateCode) {
            liveData {
                when (val response = userRepository.validateCode(it.first, it.second)) {
                    is Success -> emit(!response.value.address.isNullOrBlank())
                    is Failure -> _errorBannerMessage.value =
                        Event(loginResourceProvider.sendingActivationCodeErrorMessage)
                }
            }
        }

    fun requestCodeValidation(phoneNumber: String, activationCode: String) {
        requestActivateCode.value = Pair(phoneNumber, activationCode)
    }

    private val countDownTimer = object : CountDownTimer(CODE_EXPIRATION_TIME_SEC, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            Date(millisUntilFinished).let {
                _countDownText.value =
                    String.format(loginResourceProvider.remainingTimeExpiration, it, it)
            }
        }

        override fun onFinish() {
            _resendCodeText.value = true
        }
    }

    fun startTimer() {
        countDownTimer.start()
    }

    override fun onCleared() {
        super.onCleared()
        countDownTimer.cancel()
    }
}