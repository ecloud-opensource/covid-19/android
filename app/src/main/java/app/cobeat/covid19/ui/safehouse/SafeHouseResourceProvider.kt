package app.cobeat.covid19.ui.safehouse

data class SafeHouseResourceProvider(
    val saveSafeHouseSuccessMessage: String,
    val quarantineMapPinTitle: String
)