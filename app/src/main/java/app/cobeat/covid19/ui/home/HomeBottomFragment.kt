package app.cobeat.covid19.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import app.cobeat.covid19.R
import app.cobeat.covid19.di.HomeActivityComponentProvider
import app.cobeat.covid19.domain.CityFeed
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.ui.adapters.CityFeedAdapter
import app.cobeat.covid19.ui.adapters.CityFeedAdapterActions
import app.cobeat.covid19.util.showErrorSnackBar
import kotlinx.android.synthetic.main.fragment_home_bottom.*
import javax.inject.Inject


class HomeBottomFragment : BaseFragment(), CityFeedAdapterActions {

    @Inject
    lateinit var homeNavigationHandler: HomeNavigationHandler

    private val homeViewModel: HomeViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = HomeBottomFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home_bottom, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as HomeActivityComponentProvider)
            .getHomeActivityComponentProvider()
            .inject(this)

        setupViewElements()
    }

    private fun setupViewElements() {

        // Fellows marquee text
        fellowsCollaborating.isSelected = true

        // Error banner
        homeViewModel.errorBannerMessage.observe(viewLifecycleOwner, Observer {
            showErrorSnackBar(it)
        })

        homeViewModel.errorCityFeed.observe(viewLifecycleOwner, Observer {
            cityFeed.visibility = View.GONE
        })

        // City feed
        homeViewModel.cityFeed.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) return@Observer

            cityFeed.visibility = View.VISIBLE
            cityFeed.apply {
                setHasFixedSize(true)
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                adapter =
                    CityFeedAdapter(it, this@HomeBottomFragment)
            }
        })

        // CTAs
        emergencyButton.setOnClickListener { homeNavigationHandler.dialEmergencyNumber() }
        violationReportButton.setOnClickListener { homeNavigationHandler.dialViolationReportNumber() }
        shareButton.setOnClickListener {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                getString(R.string.colaborate_with_the_comunity)
            )
            sendIntent.type = "text/plain"
            startActivity(Intent.createChooser(sendIntent, null))
        }
        questionnaireButton.setOnClickListener {
            homeNavigationHandler.navigateToQuestionnaire()
        }

        homeViewModel.nearby.observe(viewLifecycleOwner, Observer {
            fellowsCollaborating.text = it.footerNotifications
        })
    }

    override fun onResume() {
        super.onResume()
        homeViewModel.getCityFeed()
    }

    override fun onCityFeedItemSelected(cityFeed: CityFeed) {
        homeNavigationHandler.openCityFeedUrl(cityFeed.url)
    }
}
