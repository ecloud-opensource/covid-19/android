package app.cobeat.covid19.ui.info

interface InfoNavigationHandler {
    fun navigateToInfoFragment()
    fun navigateToLink(uri: String)
}