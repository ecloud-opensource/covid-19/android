package app.cobeat.covid19.ui.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import app.cobeat.covid19.R
import app.cobeat.covid19.di.LoginActivityComponentProvider
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.ui.EventObserver
import app.cobeat.covid19.util.*
import kotlinx.android.synthetic.main.fragment_activation_code.*
import javax.inject.Inject


class ActivationCodeFragment : BaseFragment() {

    @Inject
    lateinit var loginNavigationHandler: LoginNavigationHandler
    private val activationCodeViewModel: ActivationCodeViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })
    private val loginViewModel: ValidatePhoneViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })
    private lateinit var editTextsList: List<EditText>

    companion object {

        private const val PHONE_NUMBER = "phone_number"

        @JvmStatic
        fun newInstance(phoneNumber: String) = ActivationCodeFragment().also {
            it.arguments = Bundle().also { bundle ->
                bundle.putString(PHONE_NUMBER, phoneNumber)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_activation_code, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as LoginActivityComponentProvider)
            .getLoginActivityComponentProvider()
            .inject(this)

        val phoneNumber = arguments?.getString(PHONE_NUMBER).toString()

        editTextsList =
            listOf(firstDigit, secondDigit, thirdDigit, fourthDigit, fifthDigit, sixthDigit)

        editTextsList.forEachIndexed { i, editText ->
            editText.doOnTextChanged { _, _, _, after ->
                requestNextFocus(editText, after, i)
                if (i == editTextsList.size - 1 && after > 0) {
                    showLoadingView()
                    hideKeyboard()
                    activationCodeViewModel.requestCodeValidation(
                        phoneNumber,
                        getActivationCode()
                    )
                }
            }
        }

        activationCodeViewModel.loggedInUserHasAddress.observe(viewLifecycleOwner, Observer {
            hideKeyboard()
            if (it) {
                loginNavigationHandler.navigateToHomeActivity()
            } else {
                loginNavigationHandler.navigateToSafeHouseActivity()
            }
        })

        activationCodeViewModel.errorBannerMessage.observe(viewLifecycleOwner, EventObserver {
            showErrorSnackBar(it)
            hideLoadingView()
            restartInput()
        })

        activationCodeViewModel.countDownText.observe(viewLifecycleOwner, Observer {
            countDownTextView.text = it
        })

        activationCodeViewModel.resendCodeText.observe(viewLifecycleOwner, Observer {
            processTextsVisibility(it)
        })

        loginViewModel.validatePhone.observe(viewLifecycleOwner, EventObserver { message ->
            activationCodeViewModel.startTimer()
            processTextsVisibility(false)
            hideLoadingView()
            message?.let { showSuccessSnackBar(it) }
        })

        loginViewModel.errorBannerMessage.observe(viewLifecycleOwner, EventObserver {
            hideLoadingView()
            showErrorSnackBar(it)
        })

        resendCodeTextView.setOnClickListener {
            showLoadingView()
            loginViewModel.requestPhoneValidation(phoneNumber)
        }

        tryWithOtherNumber.setOnClickListener {
            loginViewModel.clearSelectedPhoneNumber()
            loginNavigationHandler.navigateBack()
        }

        activationCodeViewModel.startTimer()
    }

    override fun onStart() {
        super.onStart()
        openKeyboardWithFirstDigit()
    }

    private fun processTextsVisibility(resendCodeVisible: Boolean) {
        resendCodeTextView.visibility = if (resendCodeVisible) View.VISIBLE else View.GONE
        countDownTextView.visibility = if (!resendCodeVisible) View.VISIBLE else View.INVISIBLE
    }

    private fun openKeyboardWithFirstDigit() {
        try {
            firstDigit.requestFocus()
            (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?)?.showSoftInput(
                firstDigit,
                InputMethodManager.SHOW_IMPLICIT
            )
        } catch (_: Exception) {
            // Silently fail ...
        }
    }

    private fun getActivationCode(): String {
        return editTextsList.fold("") { code, editText -> "${code}${editText.text}" }
    }

    private fun restartInput() {
        editTextsList.forEach { it.text.clear() }
        firstDigit.requestFocus()
    }

    private fun requestNextFocus(editText: EditText, after: Int, currentIndex: Int) {
        val view = if (after > 0)
            editText.focusSearch(View.FOCUS_RIGHT)
        else
            editText.focusSearch(View.FOCUS_LEFT)
        view?.requestFocus()
    }
}
