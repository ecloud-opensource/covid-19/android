package app.cobeat.covid19.ui.ranking

import androidx.lifecycle.*
import app.cobeat.covid19.domain.City
import app.cobeat.covid19.domain.Country
import app.cobeat.covid19.domain.RankingCountry
import app.cobeat.covid19.domain.RankingRepository
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import javax.inject.Inject

class RankingViewModel @Inject constructor(
    private val rankingRepository: RankingRepository,
    private val rankingResourceProvider: RankingResourceProvider
) : ViewModel() {

    private val _errorBannerMessage = MutableLiveData<String>()
    val errorBannerMessage: LiveData<String>
        get() = _errorBannerMessage

    private val requestRanking: MutableLiveData<Int> = MutableLiveData()
    private val requestCountry: MutableLiveData<Unit> = MutableLiveData()

    fun getRanking(id: Int) {
        requestRanking.value = id
    }

    fun getCountry() {
        requestCountry.value = Unit
    }

    val rankingList: LiveData<List<City>> =
        Transformations.switchMap(requestRanking) {
            liveData {
                when (val result = rankingRepository.getRanking(it)) {
                    is Success -> {
                        emit(result.value.cityList)
                    }
                    is Failure -> _errorBannerMessage.value =
                        result.error.message
                }
            }
        }

    val countryList: LiveData<RankingCountry> =
        Transformations.switchMap(requestCountry) {
            liveData {
                when (val result = rankingRepository.getCountry()) {
                    is Success -> {
                        emit(result.value)
                    }
                    is Failure -> _errorBannerMessage.value =
                        result.error.message
                }
            }
        }
}