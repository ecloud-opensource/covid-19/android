package app.cobeat.covid19.ui.contact

import android.os.Bundle
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.ContactActivityComponent
import app.cobeat.covid19.di.ContactActivityComponentProvider
import app.cobeat.covid19.ui.BaseActivity
import app.cobeat.covid19.util.loadFragment
import kotlinx.android.synthetic.main.toolbar_back.*

/**
 * Handles the contact section. Where the user can send messages to the administrator and developers of the app.
 */
class ContactActivity : BaseActivity(), ContactNavigationHandler, ContactActivityComponentProvider {

    private lateinit var contactActivityComponent: ContactActivityComponent

    override fun getContentViewResource() = R.layout.activity_contact

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        contactActivityComponent =
            (application as ApplicationComponentProvider).getApplicationComponent()
                .contactActivityComponentBuilder()
                .contactNavigationHandler(this)
                .contactResourceProvider(buildContactResourceProvider())
                .build()

        titleToolbar.text = resources.getString(R.string.contact_title)
        backButton.setOnClickListener { finish() }

        navigateToContactFragment()
    }

    private fun buildContactResourceProvider() = ContactResourceProvider(
        contactSuccessMessage = getString(R.string.contact_send_message_success)
    )

    override fun navigateToContactFragment() {
        loadFragment(ContactFragment.newInstance(), addToBackStack = false)
    }

    override fun navigateToHomeActivity() {
        finish()
    }

    override fun getContactActivityComponent() = contactActivityComponent
}
