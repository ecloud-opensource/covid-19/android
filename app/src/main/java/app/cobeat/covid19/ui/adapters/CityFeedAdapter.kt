package app.cobeat.covid19.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.cobeat.covid19.R
import app.cobeat.covid19.domain.CityFeed
import app.cobeat.covid19.util.ImageUtils
import kotlinx.android.synthetic.main.city_feed_item.view.*

class CityFeedAdapter(
    private val cityFeed: List<CityFeed>,
    private val listener: CityFeedAdapterActions
) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.city_feed_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val feedElement: CityFeed = cityFeed[position]

        holder.feedTitle.text = feedElement.title
        holder.feedText.text = feedElement.description
        ImageUtils.loadCenterCropImage(
            holder.feedImage.context,
            feedElement.image,
            holder.feedImage
        )

        holder.itemView.setOnClickListener { listener.onCityFeedItemSelected(feedElement) }
    }

    override fun getItemCount(): Int {
        return cityFeed.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val feedImage: ImageView = view.itemImage
    val feedTitle: TextView = view.itemTitle
    val feedText: TextView = view.itemText
}

interface CityFeedAdapterActions {
    fun onCityFeedItemSelected(cityFeed: CityFeed)
}