package app.cobeat.covid19.ui.profile

import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ProfileActivityComponentProvider
import app.cobeat.covid19.domain.User
import app.cobeat.covid19.domain.UserStatusType
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.util.hideLoadingView
import app.cobeat.covid19.util.showErrorSnackBar
import app.cobeat.covid19.util.showLoadingView
import app.cobeat.covid19.util.showSuccessSnackBar
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.view_input_text_custom.view.*
import java.util.*
import javax.inject.Inject

class ProfileFragment : BaseFragment() {

    @Inject
    lateinit var profileNavigationHandler: ProfileNavigationHandler

    private val profileViewModel: ProfileViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = ProfileFragment()
    }

    override fun onCreateView(

        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as ProfileActivityComponentProvider)
            .getProfileActivityComponent()
            .inject(this)

        profileViewModel.errorBannerMessage.observe(viewLifecycleOwner, Observer {
            hideLoadingView()
            showErrorSnackBar(it, getString(R.string.try_again),
                View.OnClickListener {
                    saveProfileInformation()
                })
        })

        profileViewModel.user.observe(viewLifecycleOwner, Observer {
            hideLoadingView()
            setUpProfileInformationFields(it)
        })

        profileViewModel.profileSaveSuccess.observe(viewLifecycleOwner, Observer {
            hideLoadingView()
            showSuccessSnackBar(it)
            saveButton.isEnabled = false
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpScreen()
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        profileViewModel.getProfile()
    }

    private fun saveProfileInformation() {
        showLoadingView()
        profileViewModel.saveProfile(
            User(
                name = firstName.getInputText(),
                lastName = lastName.getInputText(),
                email = email.getInputText(),
                phone = null,
                address = null,
                number = null,
                dni = docID.getInputText(),
                status = profileViewModel.user.value?.status
            )
        )
    }

    private fun setListeners() {
        saveButton.setOnClickListener {
            if (validateProfileInformation()) {
                saveProfileInformation()
            } else {
                showErrorSnackBar(R.string.invalid_profile_information)
            }
        }
        updateButton.setOnClickListener {
            profileNavigationHandler.navigateToQuestionnaireActivity()
        }
    }

    private fun setUpScreen() {
        firstName.setTitleText(getString(R.string.first_name))
        firstName.input.addTextChangedListener(fieldEditionWatcher)
        firstName.setTextColor(resources.getColor(R.color.colorTextDark))
        lastName.setTitleText(getString(R.string.last_name))
        lastName.input.addTextChangedListener(fieldEditionWatcher)
        lastName.setTextColor(resources.getColor(R.color.colorTextDark))
        email.setTitleText(getString(R.string.email))
        email.setInputTye(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
        email.input.addTextChangedListener(fieldEditionWatcher)
        email.setTextColor(resources.getColor(R.color.colorTextDark))
        address.setTitleText(getString(R.string.address))
        address.input.isEnabled = false
        address.setTextColor(resources.getColor(R.color.colorTextDark))
        phone.setTitleText(getString(R.string.phone))
        phone.setInputTye(InputType.TYPE_CLASS_PHONE)
        phone.input.addTextChangedListener(fieldEditionWatcher)
        phone.input.setTextColor(R.drawable.edit_text_by_state)
        phone.input.isEnabled = false
        docID.setTitleText(getString(R.string.id))
        docID.setInputTye(InputType.TYPE_CLASS_NUMBER)
        docID.input.imeOptions = EditorInfo.IME_ACTION_DONE
        docID.input.addTextChangedListener(fieldEditionWatcher)
        docID.setTextColor(resources.getColor(R.color.colorTextDark))

        addressContainer.setOnClickListener {
            profileNavigationHandler.navigateToSafeHouseActivity()
        }
    }

    private val fieldEditionWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            saveButton.isEnabled = true
        }

        override fun afterTextChanged(editable: Editable) {}
    }

    private fun setUpProfileInformationFields(user: User) {
        firstName.setInputText(user.name)
        lastName.setInputText(user.lastName)
        email.setInputText(user.email)
        address.setInputText(String.format(Locale.getDefault(), "%s %s", user.address, user.number))
        phone.setInputText(user.phone)
        docID.setInputText(user.dni)
        setUpUserStatusView(user.status)
        saveButton.isEnabled = false
    }

    private fun validateProfileInformation(): Boolean {
        return (firstName.setError(firstName.getInputText().isEmpty())) and
                (lastName.setError(lastName.getInputText().isEmpty())) and
                (email.setError(email.getInputText().isEmpty())) and
                (email.setError(
                    !android.util.Patterns.EMAIL_ADDRESS.matcher(email.getInputText()).matches()
                )) and
                (docID.setError(docID.getInputText().isEmpty()))
    }

    private fun setUpUserStatusView(status: UserStatusType?) {
        when (status) {
            UserStatusType.QUARANTINED -> {
                userStatusImage.setImageDrawable(context?.getDrawable(R.mipmap.red_status))
                userStatus.text = getString(R.string.quarantined_text)
            }
            UserStatusType.ON_RISK -> {
                userStatusImage.setImageDrawable(context?.getDrawable(R.mipmap.orange_status))
                userStatus.text = getString(R.string.risk_group)
            }
            UserStatusType.EXPOSED -> {
                userStatusImage.setImageDrawable(context?.getDrawable(R.mipmap.dark_status))
                userStatus.text = getString(R.string.contact)
            }
            UserStatusType.WITH_SYMPTOMS -> {
                userStatusImage.setImageDrawable(context?.getDrawable(R.mipmap.mustard_status))
                userStatus.text = getString(R.string.with_sintoms)
            }
            else -> {
                userStatusImage.setImageDrawable(context?.getDrawable(R.mipmap.blue_status))
                userStatus.text = getString(R.string.without_sintoms)
            }
        }
    }
}

