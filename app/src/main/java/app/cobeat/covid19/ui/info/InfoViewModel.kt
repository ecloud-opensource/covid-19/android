package app.cobeat.covid19.ui.info

import androidx.lifecycle.*
import app.cobeat.covid19.domain.links.InfoGroup
import app.cobeat.covid19.domain.links.InfoRepository
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import javax.inject.Inject

class InfoViewModel @Inject constructor(
    private val infoRepository: InfoRepository
) : ViewModel() {

    private val _errorBannerMessage = MutableLiveData<String>()
    val errorBannerMessage: LiveData<String>
        get() = _errorBannerMessage

    private val requestInfo: MutableLiveData<Unit> = MutableLiveData()

    fun getInfoLinkList() {
        requestInfo.value = Unit
    }

    val infoLinkList: LiveData<List<InfoGroup>> =
        Transformations.switchMap(requestInfo) {
            liveData {
                when (val result = infoRepository.getInfoLinkList()) {
                    is Success -> emit(result.value)
                    is Failure -> _errorBannerMessage.value = result.error.message
                }
            }
        }
}