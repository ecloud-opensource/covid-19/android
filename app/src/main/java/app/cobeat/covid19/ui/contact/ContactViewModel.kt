package app.cobeat.covid19.ui.contact

import androidx.lifecycle.*
import app.cobeat.covid19.domain.ContactMessage
import app.cobeat.covid19.domain.ContactRepository
import app.cobeat.covid19.ui.Event
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import kotlinx.coroutines.delay
import javax.inject.Inject

class ContactViewModel @Inject constructor(
    private val contactRepository: ContactRepository,
    private val contactResourceProvider: ContactResourceProvider
) : ViewModel() {

    private val _errorBannerMessage = MutableLiveData<String>()
    val errorBannerMessage: LiveData<String>
        get() = _errorBannerMessage

    private val _navigateHome = MutableLiveData<Event<Unit>>()
    val navigateHome: LiveData<Event<Unit>>
        get() = _navigateHome

    private val requestContactMessage: MutableLiveData<ContactMessage> = MutableLiveData()

    val contactMessageSuccess: LiveData<String> =
        Transformations.switchMap(requestContactMessage) {
            liveData {
                when (val result = contactRepository.sendContactMessage(it)) {
                    is Success -> {
                        emit(contactResourceProvider.contactSuccessMessage)
                        delay(2_000)
                        _navigateHome.value = Event(Unit)
                    }
                    is Failure -> _errorBannerMessage.value = result.error.message
                }
            }
        }

    fun sendContactMessage(contactMessage: ContactMessage) {
        requestContactMessage.value = contactMessage
    }
}