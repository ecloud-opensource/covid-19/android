package app.cobeat.covid19.ui.profile

data class ProfileResourceProvider(
    val saveProfileSuccessMessage: String
)