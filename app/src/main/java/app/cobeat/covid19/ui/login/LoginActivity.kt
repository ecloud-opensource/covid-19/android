package app.cobeat.covid19.ui.login

import android.content.Intent
import android.os.Bundle
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.LoginActivityComponent
import app.cobeat.covid19.di.LoginActivityComponentProvider
import app.cobeat.covid19.domain.SettingsManager
import app.cobeat.covid19.ui.BaseActivity
import app.cobeat.covid19.ui.home.HomeActivity
import app.cobeat.covid19.ui.safehouse.SafeHouseActivity
import app.cobeat.covid19.ui.safehouse.SafeHouseType
import app.cobeat.covid19.util.loadFragment
import app.cobeat.covid19.util.navigateOneStepBack
import app.cobeat.covid19.util.navigateTo
import javax.inject.Inject


/**
 * App entry point. This Activity handles the login flow. If the user is already logged in this
 * Activity will navigate directly to the [HomeActivity].
 */
class LoginActivity : BaseActivity(), LoginNavigationHandler, LoginActivityComponentProvider {

    private lateinit var loginActivityComponent: LoginActivityComponent
    @Inject
    protected lateinit var settingsManager: SettingsManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loginActivityComponent =
            (application as ApplicationComponentProvider).getApplicationComponent()
                .loginActivityComponentBuilder()
                .loginNavigationHandler(this)
                .loginResourceProvider(buildLoginResourceProvider())
                .build()

        loginActivityComponent.inject(this)

        navigateToLoginFragment()
    }

    private fun buildLoginResourceProvider() = LoginResourceProvider(
        phoneNumberError = getString(R.string.invalid_phone_number_error),
        remainingTimeExpiration = getString(R.string.login_remaining_time_expiration),
        sendingActivationCodeErrorMessage = getString(R.string.invalid_activation_code_error)
    )

    override fun navigateToActivationCodeFragment(phoneNumber: String) {
        loadFragment(ActivationCodeFragment.newInstance(phoneNumber))
    }

    override fun navigateToHomeActivity() {
        navigateTo<HomeActivity>(finish = true)
    }

    override fun navigateToSafeHouseActivity() {
        navigateTo<SafeHouseActivity>(finish = true) {
            putExtra(SafeHouseActivity.SAFE_HOUSE_TYPE, SafeHouseType.REGISTER)
        }
    }

    override fun navigateToTerms() {
        val browserIntent =
            Intent(Intent.ACTION_VIEW, settingsManager.getTermsAndConditionsUri())
        browserIntent.resolveActivity(packageManager)?.let {
            startActivity(browserIntent)
        }
    }

    override fun navigateToLoginFragment() {
        loadFragment(LoginFragment.newInstance(), addToBackStack = false)
    }

    override fun navigateBack() {
        navigateOneStepBack()
    }

    override fun getLoginActivityComponentProvider() = loginActivityComponent
}
