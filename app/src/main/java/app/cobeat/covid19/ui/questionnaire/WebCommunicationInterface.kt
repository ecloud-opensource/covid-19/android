package app.cobeat.covid19.ui.questionnaire

import android.util.Log
import android.webkit.JavascriptInterface
import app.cobeat.covid19.widgets.CustomTextView

class WebCommunicationInterface(
    private val activity: QuestionnaireActivity,
    private val titleToolbar: CustomTextView
) {

    @JavascriptInterface
    fun setTitle(title: String) {
        activity.runOnUiThread {
            titleToolbar.text = title
        }
        Log.d("TAG", "setTitle($title)")
    }

    @JavascriptInterface
    fun goToHome() {
        Log.d("TAG", "go home!")
        activity.runOnUiThread {
            activity.finish()
        }
    }
}