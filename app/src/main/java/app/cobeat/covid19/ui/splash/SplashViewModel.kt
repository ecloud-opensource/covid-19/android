package app.cobeat.covid19.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.cobeat.covid19.domain.UserRepository
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    enum class Result {
        USER_AUTHENTICATED_NO_ADDRESS, USER_AUTHENTICATED_WITH_ADDRESS, USER_NOT_AUTHENTICATED, ERROR
    }

    private val _splashDelay: Long = 2000

    private val _userAuthenticated: MutableLiveData<Result> = MutableLiveData()
    val userAuthenticated: LiveData<Result>
        get() = _userAuthenticated

    fun checkToken() {
        viewModelScope.launch {
            val startTime = Date().time
            val result = userRepository.checkIfUserIsLoggedInAndRetrieve()
            val timeUsedByRequest = Date().time - startTime
            delay(_splashDelay - timeUsedByRequest)
            _userAuthenticated.value = when (result) {
                is Success -> {
                    result.value?.let {
                        if (it.address.isNullOrEmpty()) {
                            Result.USER_AUTHENTICATED_NO_ADDRESS
                        } else {
                            Result.USER_AUTHENTICATED_WITH_ADDRESS
                        }
                    } ?: Result.USER_NOT_AUTHENTICATED
                }
                is Failure -> Result.ERROR
            }
        }
    }
}