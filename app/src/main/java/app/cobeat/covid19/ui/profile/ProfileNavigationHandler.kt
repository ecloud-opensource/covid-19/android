package app.cobeat.covid19.ui.profile

interface ProfileNavigationHandler {
    fun navigateToProfileFragment()
    fun navigateToSafeHouseActivity()
    fun navigateToQuestionnaireActivity()
}