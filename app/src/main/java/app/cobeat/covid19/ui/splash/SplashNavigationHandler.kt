package app.cobeat.covid19.ui.splash

interface SplashNavigationHandler {

    fun navigateToLoginActivity()

    fun navigateToHomeActivity()

    fun navigateToSafehouseActivity()
}