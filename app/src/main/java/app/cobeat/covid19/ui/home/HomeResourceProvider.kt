package app.cobeat.covid19.ui.home

data class HomeResourceProvider(
    val getQuarantinedUser: String,
    val getStatusOk: String,
    val homeMenuHeader: String
)