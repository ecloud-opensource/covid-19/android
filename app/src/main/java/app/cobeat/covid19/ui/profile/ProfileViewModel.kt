package app.cobeat.covid19.ui.profile

import androidx.lifecycle.*
import app.cobeat.covid19.domain.User
import app.cobeat.covid19.domain.UserRepository
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import javax.inject.Inject


class ProfileViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val profileResourceProvider: ProfileResourceProvider
) : ViewModel() {

    private val _errorBannerMessage = MutableLiveData<String>()
    val errorBannerMessage: LiveData<String>
        get() = _errorBannerMessage

    private val requestUser: MutableLiveData<Unit> = MutableLiveData()
    private val saveUser: MutableLiveData<User> = MutableLiveData()

    fun getProfile() {
        requestUser.value = Unit
    }

    val user: LiveData<User> =
        Transformations.switchMap(requestUser) {
            liveData {
                when (val result = userRepository.getUser()) {
                    is Success -> emit(result.value)
                    is Failure -> _errorBannerMessage.value = result.error.message
                }
            }
        }

    fun saveProfile(user: User) {
        saveUser.value = user
    }

    val profileSaveSuccess: LiveData<String> =
        Transformations.switchMap(saveUser) {
            liveData {
                when (val result = userRepository.updateUser(it)) {
                    is Success -> emit(profileResourceProvider.saveProfileSuccessMessage)
                    is Failure -> _errorBannerMessage.value = result.error.message
                }
            }
        }
}