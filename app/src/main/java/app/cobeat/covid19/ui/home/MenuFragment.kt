package app.cobeat.covid19.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import app.cobeat.covid19.R
import app.cobeat.covid19.di.HomeActivityComponentProvider
import app.cobeat.covid19.ui.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_menu.*
import javax.inject.Inject

class MenuFragment : Fragment() {

    @Inject
    lateinit var homeNavigationHandler: HomeNavigationHandler

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val homeViewModel: HomeViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = MenuFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as HomeActivityComponentProvider)
            .getHomeActivityComponentProvider()
            .inject(this)

        view?.let {
            button_contact.setOnClickListener {
                homeNavigationHandler.navigateToContactActivity()
            }

            button_profile.setOnClickListener {
                homeNavigationHandler.navigateToProfileActivity()
            }

            button_info.setOnClickListener {
                homeNavigationHandler.navigateToInfoActivity()
            }

            button_ranking.setOnClickListener {
                homeNavigationHandler.navigateToRankingActivity()
            }
        }
    }
}
