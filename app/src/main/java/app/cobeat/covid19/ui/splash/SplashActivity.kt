package app.cobeat.covid19.ui.splash

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import app.cobeat.covid19.BuildConfig
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.SplashActivityComponent
import app.cobeat.covid19.di.SplashActivityComponentProvider
import app.cobeat.covid19.ui.BaseActivity
import app.cobeat.covid19.ui.ViewModelFactory
import app.cobeat.covid19.ui.home.HomeActivity
import app.cobeat.covid19.ui.login.LoginActivity
import app.cobeat.covid19.ui.safehouse.SafeHouseActivity
import app.cobeat.covid19.ui.safehouse.SafeHouseActivity.Companion.SAFE_HOUSE_TYPE
import app.cobeat.covid19.ui.safehouse.SafeHouseType
import app.cobeat.covid19.util.navigateTo
import app.cobeat.covid19.util.showErrorSnackBar
import kotlinx.android.synthetic.main.activity_splash.*
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashActivityComponentProvider, SplashNavigationHandler {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val splashViewModel: SplashViewModel
            by viewModels(
                factoryProducer = { viewModelFactory })

    private lateinit var splashActivityComponent: SplashActivityComponent

    override fun getContentViewResource() = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        splashActivityComponent =
            (application as ApplicationComponentProvider).getApplicationComponent()
                .splashActivityComponentBuilder()
                .loginNavigationHandler(this)
                .build()

        getSplashActivityComponentProvider()
            .inject(this)

        appVersion.text = String.format(getString(R.string.version_name), BuildConfig.VERSION_NAME)

        splashViewModel.userAuthenticated.observe(this, Observer {
            when (it) {
                SplashViewModel.Result.USER_AUTHENTICATED_WITH_ADDRESS -> navigateToHomeActivity()
                SplashViewModel.Result.USER_AUTHENTICATED_NO_ADDRESS -> navigateToSafehouseActivity()
                SplashViewModel.Result.USER_NOT_AUTHENTICATED -> navigateToLoginActivity()
                SplashViewModel.Result.ERROR -> showErrorSnackBar(
                    getString(R.string.generic_error),
                    getString(R.string.close),
                    View.OnClickListener {
                        finish()
                    })
            }
        })

        splashViewModel.checkToken()
    }

    override fun navigateToLoginActivity() {
        navigateTo<LoginActivity>(finish = true)
    }

    override fun navigateToHomeActivity() {
        navigateTo<HomeActivity>(finish = true)
    }

    override fun navigateToSafehouseActivity() {
        navigateTo<SafeHouseActivity>(finish = true) {
            putExtra(SAFE_HOUSE_TYPE, SafeHouseType.REGISTER)
        }
    }

    override fun getSplashActivityComponentProvider(): SplashActivityComponent {
        return splashActivityComponent
    }
}
