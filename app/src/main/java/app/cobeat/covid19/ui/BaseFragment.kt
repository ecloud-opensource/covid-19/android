package app.cobeat.covid19.ui

import androidx.fragment.app.Fragment
import javax.inject.Inject

open class BaseFragment : Fragment() {
    @Inject
    protected lateinit var viewModelFactory: ViewModelFactory
}