package app.cobeat.covid19.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.cobeat.covid19.R
import app.cobeat.covid19.domain.City
import app.cobeat.covid19.widgets.CustomTextView
import kotlinx.android.synthetic.main.ranking_item.view.*

class RankingAdapter() : RecyclerView.Adapter<RankingViewHolder>() {

    private var rankingList: List<City> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RankingViewHolder {
        return RankingViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.ranking_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return rankingList.size
    }

    override fun onBindViewHolder(holder: RankingViewHolder, position: Int) {
        val rankingElement: City = rankingList[position]

        holder.cityTitle.text = rankingElement.name
        holder.collaboratorsText.text = rankingElement.fellows.toString()
        holder.quarantineText.text = rankingElement.areInfected.toString()
        holder.itemView.setBackgroundColor(
            holder.itemView.context.resources.getColor(if (position % 2 == 0) R.color.colorWhite else R.color.colorAccentOpacity)
        )
    }

    fun setRankingList(list: List<City>) {
        rankingList = list
        notifyDataSetChanged()
    }
}


class RankingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val cityTitle: CustomTextView = view.cityTitle
    val collaboratorsText: CustomTextView = view.collaboratorsText
    val quarantineText: CustomTextView = view.quarantineText
}