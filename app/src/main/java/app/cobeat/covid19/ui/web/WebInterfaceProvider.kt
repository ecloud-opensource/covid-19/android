package app.cobeat.covid19.ui.web

/** Interface provider to communicate natively from the web */
interface WebInterfaceProvider<T> {
    fun getWebInterface(): T
}