package app.cobeat.covid19.ui.contact

import android.animation.Animator
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ContactActivityComponentProvider
import app.cobeat.covid19.domain.ContactMessage
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.ui.EventObserver
import app.cobeat.covid19.util.*
import kotlinx.android.synthetic.main.fragment_contact.*
import kotlinx.android.synthetic.main.view_input_text_custom.view.*
import java.util.*
import javax.inject.Inject

class ContactFragment : BaseFragment() {

    @Inject
    lateinit var contactNavigationHandler: ContactNavigationHandler

    private val contactViewModel: ContactViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = ContactFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as ContactActivityComponentProvider)
            .getContactActivityComponent()
            .inject(this)

        contactViewModel.errorBannerMessage.observe(viewLifecycleOwner, Observer {
            hideLoadingView()
            showErrorSnackBar(it)
        })

        contactViewModel.contactMessageSuccess.observe(viewLifecycleOwner, Observer {
            hideLoadingView()
            showSuccessSnackBar(it)
        })

        contactViewModel.navigateHome.observe(viewLifecycleOwner, EventObserver {
            contactNavigationHandler.navigateToHomeActivity()
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpScreen()
        setListeners()
    }

    private fun setListeners() {
        sendInformationButton.setOnClickListener {
            sendMessageInformation()
        }
        professionalContact.setOnClickListener { context?.let { MailHelper.sendEmailToSupport(it) } }
    }

    private fun sendMessageInformation() {
        if (validateInformation()) {
            sendContactMessage()
        } else {
            showErrorSnackBar(getString(R.string.invalid_profile_information))
        }
    }

    private fun sendContactMessage() {
        showLoadingView()
        contactViewModel.sendContactMessage(
            ContactMessage(
                name = firstName.input.text.toString(),
                lastName = lastName.input.text.toString(),
                email = email.input.text.toString(),
                message = inputMessage.text.toString()
            )
        )
    }

    private fun setUpScreen() {
        firstName.setTitleText(getString(R.string.first_name))
        lastName.setTitleText(getString(R.string.last_name))
        email.setTitleText(getString(R.string.email))
        email.setInputTye(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
        inputMessage.imeOptions = EditorInfo.IME_ACTION_DONE
        inputMessage.setRawInputType(InputType.TYPE_CLASS_TEXT)
        inputMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                setCharCounterView(p0.toString().length)
            }
        })
        inputMessage.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(p0: TextView?, actionId: Int, p2: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    context?.let {
                        val imm =
                            it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(view?.windowToken, 0)
                    }
                    sendMessageInformation()
                    return true
                }
                return false
            }
        })
    }

    private fun validateInformation(): Boolean {
        return (firstName.setError(firstName.getInputText().isEmpty())) and
                (lastName.setError(lastName.getInputText().isEmpty())) and
                (email.setError(email.getInputText().isEmpty())) and
                (email.setError(
                    !android.util.Patterns.EMAIL_ADDRESS.matcher(email.getInputText()).matches()
                )) and (validateMessage())
    }

    private fun validateMessage(): Boolean {
        if (inputMessage.text.toString().isEmpty()) {
            errorMessage?.animate()?.alpha(1.0f)?.setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(p0: Animator?) {

                }

                override fun onAnimationEnd(p0: Animator?) {
                    errorMessage?.animate()?.alpha(0.0f)?.duration = 4000
                }

                override fun onAnimationCancel(p0: Animator?) {

                }

                override fun onAnimationStart(p0: Animator?) {

                }

            })?.duration = 2000
            return false
        }
        return true
    }

    private fun setCharCounterView(number: Int) {
        charCounter.text = String.format(Locale.getDefault(), "%d/140", number)
    }
}