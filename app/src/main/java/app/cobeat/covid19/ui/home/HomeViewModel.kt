package app.cobeat.covid19.ui.home

import android.location.Location
import android.text.TextUtils
import androidx.lifecycle.*
import app.cobeat.covid19.domain.*
import app.cobeat.covid19.ui.LocationHandler
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val homeResourceProvider: HomeResourceProvider,
    locationHandler: LocationHandler
) : ViewModel() {
    init {
        locationHandler.locationListener = {
            if (it is Success) {
                _lastLocation.value = it.value
            }
        }
    }

    private val _lastLocation = MutableLiveData<Location>()
    val lastLocation: LiveData<Location>
        get() = _lastLocation

    private val _errorBannerMessage = MutableLiveData<String>()
    val errorBannerMessage: LiveData<String>
        get() = _errorBannerMessage

    private val _errorCityFeed = MutableLiveData<Unit>()
    val errorCityFeed: LiveData<Unit>
        get() = _errorCityFeed

    private val requestCityFeed: MutableLiveData<Unit> = MutableLiveData()
    private val requestNearby: MutableLiveData<MapArea> = MutableLiveData()

    fun getCityFeed() {
        requestCityFeed.value = Unit
    }

    fun getNearby(mapAreaUi: MapArea) {
        requestNearby.value = mapAreaUi
    }

    val cityFeed: LiveData<List<CityFeed>> =
        Transformations.switchMap(requestCityFeed) {
            liveData {
                when (val result = homeRepository.getCityFeed()) {
                    is Success -> emit(result.value)
                    is Failure -> _errorBannerMessage.value = result.error.message
                }
            }
        }

    val nearby: LiveData<NearbyResultsUi> =
        Transformations.switchMap(requestNearby) {
            liveData {
                when (val result = homeRepository.getNearby(it)) {
                    is Success -> emit(transformNearbyInfoToNearbyResultsUi(result.value))
                    is Failure -> _errorBannerMessage.value = result.error.message
                }
            }
        }

    private fun transformNearbyInfoToNearbyResultsUi(nearbyInfo: NearbyInfo): NearbyResultsUi {
        val markers = nearbyInfo.users.map {
            NearbyResultsUi.UserMarkers(
                isQuarantined = it.isQuarantined,
                status = if (it.isQuarantined) homeResourceProvider.getQuarantinedUser
                else homeResourceProvider.getStatusOk,
                latLng = it.latestPoint.coordinates
            )
        }

        val headerNotification = nearbyInfo.notification.firstOrNull {
            it.type == Notification.HEADER
        }?.let {
            String.format(it.title, nearbyInfo.users.count())
        }

        val footerNotifications = nearbyInfo.notification.filter {
            it.type == Notification.FOOTER
        }.map { it.title }.let { TextUtils.join(" | ", it) }

        return NearbyResultsUi(
            headerNotification = headerNotification,
            footerNotifications = footerNotifications,
            markers = markers
        )
    }

    private val cityRequest = MutableLiveData<Unit>()
    val homeMenuHeader: LiveData<String> =
        Transformations.switchMap(cityRequest) {
            liveData {
                val result = homeRepository.getCity()
                if (result is Success) {
                    emit(getHomeMenuHeaderText(result.value))
                } else {
                    emit("")
                }
            }
        }

    fun populateHomeMenuHeader() {
        cityRequest.value = Unit
    }

    private fun getHomeMenuHeaderText(city: City): String {
        return String.format(
            homeResourceProvider.homeMenuHeader,
            city.fellows,
            city.areInfected
        )
    }
}