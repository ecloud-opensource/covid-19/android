package app.cobeat.covid19.ui.ranking

import android.os.Bundle
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.RankingActivityComponent
import app.cobeat.covid19.di.RankingActivityComponentProvider
import app.cobeat.covid19.ui.BaseActivity
import app.cobeat.covid19.util.loadFragment
import kotlinx.android.synthetic.main.toolbar_back.*

class RankingActivity : BaseActivity(), RankingNavigationHandler, RankingActivityComponentProvider {

    private lateinit var rankingActivityComponent: RankingActivityComponent

    override fun getContentViewResource() = R.layout.activity_ranking

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        rankingActivityComponent =
            (application as ApplicationComponentProvider)
                .getApplicationComponent()
                .rankingActivityComponentBuilder()
                .rankingNavigationHandler(this)
                .rankingResourceProvider(buildRankingResourceProvider())
                .build()

        titleToolbar.text = resources.getString(R.string.ranking_title)
        backButton.setOnClickListener { finish() }

        navigateToRankingFragment()
    }

    // TODO: Remove this if empty ..
    private fun buildRankingResourceProvider() =
        RankingResourceProvider(
            rankingLoadingErrorMessage = ""
        )

    override fun navigateToRankingFragment() {
        loadFragment(RankingFragment.newInstance(), addToBackStack = false)
    }

    override fun getRankingActivityComponent() = rankingActivityComponent
}
