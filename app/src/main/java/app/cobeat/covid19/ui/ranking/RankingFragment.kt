package app.cobeat.covid19.ui.ranking

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import app.cobeat.covid19.R
import app.cobeat.covid19.di.RankingActivityComponentProvider
import app.cobeat.covid19.domain.City
import app.cobeat.covid19.domain.Country
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.ui.adapters.RankingAdapter
import app.cobeat.covid19.util.hideLoadingView
import app.cobeat.covid19.util.showErrorSnackBar
import app.cobeat.covid19.util.showLoadingView
import kotlinx.android.synthetic.main.fragment_ranking.*
import org.angmarch.views.OnSpinnerItemSelectedListener


class RankingFragment : BaseFragment() {

    private val rankingViewModel: RankingViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    private var rankingAdapter: RankingAdapter? = null

    companion object {
        @JvmStatic
        fun newInstance() = RankingFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ranking, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as RankingActivityComponentProvider)
            .getRankingActivityComponent()
            .inject(this)

        setUpRankingList()

        rankingViewModel.errorBannerMessage.observe(viewLifecycleOwner, Observer {
            hideLoadingView()
            showErrorSnackBar(it)
        })

        rankingViewModel.rankingList.observe(viewLifecycleOwner, Observer {
            hideLoadingView()
            setUpCities(it)
        })

        rankingViewModel.countryList.observe(viewLifecycleOwner, Observer {
            setUpCountrySpinner(it.countryList)
            setUpInfo(it.countryList)
        })

        showLoadingView()
        rankingViewModel.getCountry()
    }

    private fun setUpInfo(countryList: List<Country>) {
        countryList.firstOrNull().let { country ->
            country?.id?.let {
                rankingViewModel.getRanking(it)
            }
            country?.cities?.let { setUpCitySpinner(it) }
        }
    }

    private fun setUpRankingList() {
        rankingList.apply {
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            rankingAdapter = RankingAdapter()
            adapter = rankingAdapter
        }
    }

    private fun setUpCities(rankingCityList: List<City>) {
        rankingAdapter?.setRankingList(rankingCityList)
    }

    private fun setUpCountrySpinner(countryList: List<Country>) {
        val countries = arrayListOf<String>()
        for (item in countryList) {
            countries.add(item.name)
        }
        countrySpinner.attachDataSource(countries)
        countrySpinner.selectedIndex = 0
        countrySpinner.setBackgroundColor(resources.getColor(android.R.color.transparent))
        countrySpinner.onSpinnerItemSelectedListener =
            OnSpinnerItemSelectedListener { parent, view, position, id ->
                val item = parent.getItemAtPosition(position)
                getCountryByName(item.toString())?.let {
                    rankingViewModel.getRanking(it.id)
                }
            }
    }

    private fun setUpCitySpinner(cityList: List<City>) {
        val cities = arrayListOf<String>()
        cities.add(0, getString(R.string.all_cities_label))
        for (item in cityList) {
            item.name?.let { cities.add(it) }
        }
        citySpinner.attachDataSource(cities)
        citySpinner.selectedIndex = 0
        citySpinner.setBackgroundColor(resources.getColor(android.R.color.transparent))
        citySpinner.onSpinnerItemSelectedListener =
            OnSpinnerItemSelectedListener { parent, view, position, id ->
                val item = parent.getItemAtPosition(position)
                if (position > 0) {
                    getCityByName(item.toString())?.let {
                        setUpCities(listOf(it))
                    }
                } else {
                    rankingViewModel.rankingList.value?.let { setUpCities(it) }
                }
            }
    }

    private fun getCityByName(name: String): City? {
        rankingViewModel.rankingList.value?.let {
            for (city in it) {
                if (city.name == name) return city
            }
        }
        return null
    }

    private fun getCountryByName(name: String): Country? {
        rankingViewModel.countryList.value?.countryList?.let {
            for (country in it) {
                if (country.name == name) return country
            }
        }
        return null
    }
}

