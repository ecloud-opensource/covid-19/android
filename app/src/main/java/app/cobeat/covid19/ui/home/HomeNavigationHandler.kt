package app.cobeat.covid19.ui.home

interface HomeNavigationHandler {

    fun navigateToContactActivity()

    fun navigateToQuestionnaire()

    fun navigateToInfoActivity()

    fun navigateToProfileActivity()

    fun navigateToRankingActivity()

    fun openCityFeedUrl(url: String)

    fun dialEmergencyNumber()

    fun dialViolationReportNumber()
}