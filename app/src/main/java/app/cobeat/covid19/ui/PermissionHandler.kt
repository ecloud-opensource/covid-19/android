package app.cobeat.covid19.ui

import android.Manifest
import android.app.Activity
import android.os.Build
import androidx.core.app.ActivityCompat
import app.cobeat.covid19.util.checkPermissionGranted
import app.cobeat.covid19.util.hasBackgroundLocationPermission
import app.cobeat.covid19.util.hasForegroundLocationPermission

/**
 * Helper class for the android permission handling.
 *
 * This class should be only referenced by the [Activity] or other classes that live along with the host [Activity].
 */
class PermissionHandler(private val activity: Activity) {

    companion object {
        const val MY_PERMISSIONS_REQUEST_LOCATION = 100
    }

    fun askLocationPermissions() {
        if (!hasForegroundLocation() && !hasBackgroundLocation()) {
            requestLocationPermissions()
        }

        checkAndAskPermissionIfNecessaryByString(Manifest.permission.ACCESS_FINE_LOCATION)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            checkAndAskPermissionIfNecessaryByString(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        }
    }

    private fun checkAndAskPermissionIfNecessaryByString(permission: String) {
        if (!checkPermissionGranted(activity, permission)) {
            requestPermissionRationaleByString(permission)
        }
    }

    private fun requestPermissionRationaleByString(permission: String) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                activity,
                permission
            )
        ) {
            //TODO Handle this scenario.
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            requestPermissionByString(permission)
        } else {
            requestPermissionByString(permission)
        }
    }

    private fun requestPermissionByString(permission: String) {
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(permission),
            MY_PERMISSIONS_REQUEST_LOCATION
        )
    }

    private fun requestLocationPermissions() {
        val permissionList = mutableListOf(Manifest.permission.ACCESS_FINE_LOCATION)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissionList.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        }

        if (ActivityCompat.shouldShowRequestPermissionRationale(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            //TODO Handle this scenario.
            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            ActivityCompat.requestPermissions(
                activity,
                permissionList.toTypedArray(),
                MY_PERMISSIONS_REQUEST_LOCATION
            )
        } else {
            ActivityCompat.requestPermissions(
                activity,
                permissionList.toTypedArray(),
                MY_PERMISSIONS_REQUEST_LOCATION
            )
        }
    }

    fun isForegroundLocationPermissionGranted(requestCode: Int): Boolean {
        return requestCode == MY_PERMISSIONS_REQUEST_LOCATION
                && hasForegroundLocation()
    }

    fun isBackgroundLocationPermissionGranted(requestCode: Int): Boolean {
        return requestCode == MY_PERMISSIONS_REQUEST_LOCATION
                && hasBackgroundLocation()
    }

    fun hasForegroundLocation() = hasForegroundLocationPermission(activity)

    fun hasBackgroundLocation() = hasBackgroundLocationPermission(activity)
}