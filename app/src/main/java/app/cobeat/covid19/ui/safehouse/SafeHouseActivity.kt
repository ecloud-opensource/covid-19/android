package app.cobeat.covid19.ui.safehouse

import android.app.Activity
import android.os.Bundle
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.SafeHouseActivityComponent
import app.cobeat.covid19.di.SafehouseActivityComponentProvider
import app.cobeat.covid19.network.tracking.LocationTrackingWorker
import app.cobeat.covid19.ui.BaseActivity
import app.cobeat.covid19.ui.LocationHandler
import app.cobeat.covid19.ui.MapHandler
import app.cobeat.covid19.ui.PermissionHandler
import app.cobeat.covid19.ui.home.HomeActivity
import app.cobeat.covid19.util.loadFragment
import app.cobeat.covid19.util.navigateTo
import java.io.Serializable


class SafeHouseActivity : BaseActivity(), SafeHouseNavigationHandler,
    SafehouseActivityComponentProvider {

    private lateinit var safeHouseActivityComponent: SafeHouseActivityComponent
    private lateinit var permissionHandler: PermissionHandler
    private lateinit var locationHandler: LocationHandler
    private lateinit var mapHandler: MapHandler

    companion object {
        const val SAFE_HOUSE_TYPE = "SAFE_HOUSE_TYPE"
    }

    override fun getContentViewResource() = R.layout.activity_safehouse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        locationHandler = LocationHandler(this)
        permissionHandler = PermissionHandler(this)
        mapHandler = MapHandler(this, locationHandler)

        safeHouseActivityComponent =
            (application as ApplicationComponentProvider).getApplicationComponent()
                .safeHouseActivityComponentBuilder()
                .safehouseNavigationHandler(this)
                .locationHandler(locationHandler)
                .permissionHandler(permissionHandler)
                .mapHandler(mapHandler)
                .placesHandler(PlacesHandler(this))
                .safeHouseResourceProvider(buildSafeHouseResourceProvider())
                .build()

        permissionHandler.askLocationPermissions()

        loadFragment(
            fragment = SafeHouseFragment.newInstance(),
            addToBackStack = false,
            container = R.id.main_container
        )
        loadFragment(
            fragment = SafeHouseBottomFragment.newInstance(),
            addToBackStack = false,
            container = R.id.bottom_container
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissionHandler.isForegroundLocationPermissionGranted(requestCode)) {
            mapHandler.displayMyLocation()
        }

        if (permissionHandler.isBackgroundLocationPermissionGranted(requestCode)) {
            LocationTrackingWorker.startWorker(this)
        }

        //TODO Handle the scenario where the user doesn't enable the location
    }

    override fun getSafehouseActivityComponentProvider() = safeHouseActivityComponent

    private fun buildSafeHouseResourceProvider() =
        SafeHouseResourceProvider(
            saveSafeHouseSuccessMessage = getString(R.string.safe_house_save_success_message),
            quarantineMapPinTitle = getString(R.string.quarantine_map_pin_title)
        )

    override fun navigateToHomeActivity() {
        navigateTo<HomeActivity>(finish = true)
    }

    override fun navigateToProfileActivity() {
        finish()
    }

    override fun onSafeHouseSaved() {
        setResult(Activity.RESULT_OK)
        if (intent.extras?.getSerializable(SAFE_HOUSE_TYPE) == SafeHouseType.EDIT) {
            navigateToProfileActivity()
        } else {
            navigateToHomeActivity()
        }
    }
}

enum class SafeHouseType : Serializable {
    REGISTER, EDIT
}