package app.cobeat.covid19.ui.login

import androidx.lifecycle.*
import app.cobeat.covid19.domain.Registration
import app.cobeat.covid19.domain.UserRepository
import app.cobeat.covid19.ui.Event
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import javax.inject.Inject

class ValidatePhoneViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val loginResourceProvider: LoginResourceProvider
) : ViewModel() {

    var selectedPhoneNumber: String = ""
        private set

    companion object {
        private val phoneRegex = Regex("^[+][0-9]{10,13}$")
    }

    private val _errorBannerMessage = MutableLiveData<Event<String>>()
    val errorBannerMessage: LiveData<Event<String>>
        get() = _errorBannerMessage

    private val _clearPhoneNumber = MutableLiveData<Event<Unit>>()
    val clearPhoneNumber: LiveData<Event<Unit>>
        get() = _clearPhoneNumber

    private var requestLogin: MutableLiveData<Registration> = MutableLiveData()

    var validatePhone: LiveData<Event<String?>> =
        Transformations.switchMap(requestLogin) {
            liveData {
                when (val result = userRepository.validatePhone(it)) {
                    is Success -> {
                        emit(Event(result.value))
                    }
                    is Failure -> {
                        handleError(result.error)
                    }
                }
            }
        }

    fun requestPhoneValidation(phoneNumber: String) {
        this.selectedPhoneNumber = phoneNumber
        if (isPhoneNumberValid(phoneNumber)) {
            requestLogin.value = Registration(phoneNumber)
        } else {
            handleError(Error(loginResourceProvider.phoneNumberError))
        }
    }

    private fun handleError(error: Throwable) {
        _errorBannerMessage.value = Event(error.message.toString())
    }

    private fun isPhoneNumberValid(number: String) = number.matches(phoneRegex)

    fun clearSelectedPhoneNumber() {
        selectedPhoneNumber = ""
        _clearPhoneNumber.value = Event(Unit)
    }
}