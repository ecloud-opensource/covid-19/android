package app.cobeat.covid19.ui

import android.app.Activity
import android.location.Location
import android.util.Log
import app.cobeat.covid19.R
import app.cobeat.covid19.domain.MapArea
import app.cobeat.covid19.util.hasForegroundLocationPermission
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions

/**
 * Wraps the interactions with [GoogleMap].
 *
 * This class should be only referenced by the [Activity] or other classes that live along with the host [Activity].
 */
class MapHandler(
    private val activity: Activity,
    private val locationHandler: LocationHandler
) : OnMapReadyCallback {

    private var map: GoogleMap? = null
    var onCameraIdleListener: ((MapArea) -> Unit)? = null
    var onCameraMoveListener: (() -> Unit)? = null

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap
        map?.apply {
            setMapStyle(
                MapStyleOptions.loadRawResourceStyle(activity, R.raw.map_style)
            )
            uiSettings.isMyLocationButtonEnabled = false
            displayMyLocation()
            uiSettings.isZoomGesturesEnabled = true
            uiSettings.isZoomControlsEnabled = true
            setPadding(
                0,
                activity.resources.getDimensionPixelOffset(R.dimen.maps_padding_height),
                0,
                activity.resources.getDimensionPixelOffset(R.dimen.maps_padding_bottom)
            )

            setOnCameraMoveListener {
                onCameraMoveListener?.let { listener ->
                    listener()
                }
            }
            setOnCameraIdleListener {
                onCameraIdleListener?.let { listener ->
                    this.projection.visibleRegion.latLngBounds.let {
                        listener(
                            calculateMapPoints(
                                northeast = it.northeast,
                                southwest = it.southwest
                            )
                        )
                    }
                }
            }
        }
    }

    fun moveCameraToLocation(location: Location) {
        moveCameraToLocation(location.latitude, location.longitude)
    }

    fun moveCameraToLocation(latitude: Double, longitude: Double) {
        map?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(latitude, longitude),
                15f
            )
        )
    }

    fun displayMyLocation() {
        if (hasForegroundLocationPermission(activity)) {
            map?.isMyLocationEnabled = true
            map?.uiSettings?.isMyLocationButtonEnabled = true
            locationHandler.getLastLocationAndNotifyListener()
        }
    }

    fun addMarker(latLng: LatLng, title: String, markerIcon: Int) {
        map?.addMarker(
            MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(markerIcon))
                .position(latLng)
                .title(title)
        )
    }

    fun clearMarkers() {
        map?.clear()
    }

    private fun calculateMapPoints(northeast: LatLng, southwest: LatLng): MapArea {
        val northWest = LatLng(northeast.latitude, southwest.longitude)
        val southEast = LatLng(southwest.latitude, northeast.longitude)
        Log.d(
            "\nMap visibleRegion:\n",
            "northWest: $northWest\n" +
                    "northEast: ${northeast}\n" +
                    "southWest: ${southwest}\n" +
                    "southEast: $southEast"
        )

        return MapArea(
            northWest = northWest,
            northEast = northeast,
            southWest = southwest,
            southEast = southEast
        )
    }

}