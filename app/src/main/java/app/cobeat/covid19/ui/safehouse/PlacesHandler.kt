package app.cobeat.covid19.ui.safehouse

import android.app.Activity
import app.cobeat.covid19.R
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient

const val GOOGLE_PLACES_DEFAULT_COUNTRY = "AR"

/**
 * Wraps the interactions with [Places].
 *
 * This class should be only referenced by the [Activity] or other classes that live along with the host [Activity].
 */
class PlacesHandler(activity: Activity) {
    private val placesClient: PlacesClient

    init {
        Places.initialize(activity.applicationContext, activity.getString(R.string.google_maps_key))
        placesClient = Places.createClient(activity)
    }

    private val currentPlaceFieldList = listOf(
        Place.Field.ID,
        Place.Field.NAME,
        Place.Field.ADDRESS,
        Place.Field.LAT_LNG
    )
    private val placeFieldList = currentPlaceFieldList.plus(Place.Field.ADDRESS_COMPONENTS)

    fun findCurrentPlace(onPlaceResponse: (SelectedPlace) -> Unit) {
        placesClient.findCurrentPlace(
            FindCurrentPlaceRequest.newInstance(currentPlaceFieldList)
        ).addOnSuccessListener {
            it?.let { response ->
                response.placeLikelihoods
                    .getOrNull(0)?.apply {
                        place.id?.let { placeId ->
                            placesClient.fetchPlace(
                                FetchPlaceRequest.newInstance(
                                    placeId,
                                    placeFieldList
                                )
                            ).addOnSuccessListener { fetchPlaceResponse ->
                                onPlaceResponse(transformToSelectedPlace(fetchPlaceResponse.place))
                            }
                        }
                    }
            }
        }
    }

    fun transformToSelectedPlace(place: Place): SelectedPlace {
        var streetName = ""
        var streetNumber = ""
        place.addressComponents?.asList()?.forEach {
            it.types.forEach { type ->
                if (type == "route") {
                    streetName = it.name
                } else if (type == "street_number") {
                    streetNumber = it.name
                }
            }
        }

        val searchQuery = place.address?.split(",")
            ?.getOrElse(0) { "" } ?: ""

        return SelectedPlace(place, streetName, streetNumber, searchQuery)
    }

}