package app.cobeat.covid19.ui.questionnaire

import android.os.Bundle
import app.cobeat.covid19.BuildConfig
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.storage.SecureStorage
import app.cobeat.covid19.ui.web.WebActivity
import app.cobeat.covid19.ui.web.WebInterfaceProvider
import kotlinx.android.synthetic.main.toolbar_back.*
import javax.inject.Inject

class QuestionnaireActivity : WebActivity(), WebInterfaceProvider<WebCommunicationInterface> {

    @Inject
    lateinit var secureStorage: SecureStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as ApplicationComponentProvider)
            .getApplicationComponent()
            .questionnaireActivityComponentBuilder()
            .build()
            .inject(this)

        titleToolbar.text = resources.getString(R.string.questionnaire)

        secureStorage.getUserToken()?.let {
            val uri = BuildConfig.WEB_URL_QUESTIONNAIRE.plus("?x-api-key=$it")
            loadWeb(uri)
        }
    }

    override fun getWebInterface(): WebCommunicationInterface {
        return WebCommunicationInterface(this, titleToolbar)
    }

}