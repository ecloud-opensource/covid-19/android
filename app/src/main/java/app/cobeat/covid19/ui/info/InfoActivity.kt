package app.cobeat.covid19.ui.info

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.InfoActivityComponent
import app.cobeat.covid19.di.InfoActivityComponentProvider
import app.cobeat.covid19.ui.BaseActivity
import app.cobeat.covid19.util.loadFragment
import kotlinx.android.synthetic.main.toolbar_back.*

/**
 * Handles the "Links" section of the app. This section may allocate more information than links
 * in the near future.
 */
class InfoActivity : BaseActivity(), InfoActivityComponentProvider, InfoNavigationHandler {

    private lateinit var infoActivityComponent: InfoActivityComponent

    override fun getContentViewResource() = R.layout.activity_info

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        infoActivityComponent = (application as ApplicationComponentProvider)
            .getApplicationComponent()
            .infoActivityComponentBuilder()
            .infoNavigationHandler(this)
            .build()

        titleToolbar.text = getString(R.string.links_section_title)
        backButton.setOnClickListener { finish() }
        navigateToInfoFragment()
    }

    override fun getInfoActivityComponent() = infoActivityComponent

    override fun navigateToInfoFragment() {
        loadFragment(InfoFragment.newInstance(), addToBackStack = false)
    }

    override fun navigateToLink(uri: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        browserIntent.resolveActivity(packageManager)?.let {
            startActivity(browserIntent)
        }
    }
}
