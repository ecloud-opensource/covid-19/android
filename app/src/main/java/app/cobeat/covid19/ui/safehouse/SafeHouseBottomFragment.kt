package app.cobeat.covid19.ui.safehouse

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import app.cobeat.covid19.R
import app.cobeat.covid19.di.SafehouseActivityComponentProvider
import app.cobeat.covid19.domain.SafeHouse
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.ui.EventObserver
import app.cobeat.covid19.util.hideLoadingView
import app.cobeat.covid19.util.showErrorSnackBar
import app.cobeat.covid19.util.showLoadingView
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import kotlinx.android.synthetic.main.fragment_safehouse_bottom.*
import javax.inject.Inject


class SafeHouseBottomFragment : BaseFragment() {

    private var selectedPlace: SelectedPlace? = null

    @Inject
    lateinit var safeHouseNavigationHandler: SafeHouseNavigationHandler

    private val safeHouseViewModel: SafeHouseViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = SafeHouseBottomFragment()

        private const val AUTOCOMPLETE_REQUEST_CODE = 13
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_safehouse_bottom, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as SafehouseActivityComponentProvider)
            .getSafehouseActivityComponentProvider()
            .inject(this)

        safeHouseViewModel.safeHouseSaveSuccess.observe(viewLifecycleOwner, EventObserver {
            hideLoadingView()
            safeHouseNavigationHandler.onSafeHouseSaved()
        })

        safeHouseViewModel.errorBannerMessage.observe(viewLifecycleOwner, EventObserver {
            hideLoadingView()
            showErrorSnackBar(it)
        })

        safeHouseViewModel.currentPlace.observe(viewLifecycleOwner, EventObserver {
            selectedPlace = it
            collaborateNow.isEnabled = true
            collaborateNow.alpha = 1f
            clickToAddLocation.visibility = GONE
            placeSelected.visibility = VISIBLE
            placeSelected.text = it.place.name
            street.text = it.searchQuery

            hideLoadingView()
        })

        safeHouseViewModel.locationUpdateError.observe(viewLifecycleOwner, EventObserver {
            hideLoadingView()
        })

        if (safeHouseViewModel.obtainLastLocation()) {
            showLoadingView()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (data != null && resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data)
                safeHouseViewModel.onNewPlaceSelected(place)
            }
        }
    }

    private fun setUpView() {
        collaborateNow.isEnabled = false
        street.setOnClickListener {
            navigateToPlaces()
        }

        collaborateNow.setOnClickListener {
            if (safeHouseViewModel.isValidPlace(selectedPlace)) {
                showLoadingView()
                saveSafeHouse()
            } else {
                showErrorSnackBar(getString(R.string.invalid_address))
            }
        }
    }

    private val placeFields = listOf(
        Place.Field.ID,
        Place.Field.NAME,
        Place.Field.ADDRESS,
        Place.Field.ADDRESS_COMPONENTS,
        Place.Field.LAT_LNG
    )

    private fun navigateToPlaces() {
        val intent =
            Autocomplete
                .IntentBuilder(
                    AutocompleteActivityMode.OVERLAY, placeFields
                )
                .setTypeFilter(TypeFilter.ADDRESS)
                .setCountry(GOOGLE_PLACES_DEFAULT_COUNTRY)
                .apply {
                    selectedPlace?.let {
                        setInitialQuery(it.searchQuery)
                    }
                }
                .build(requireActivity())
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    private fun saveSafeHouse() {
        selectedPlace?.let {
            it.place.latLng?.apply {
                safeHouseViewModel.saveSafeHouse(
                    SafeHouse(
                        address = it.streetName,
                        number = it.streetNumber,
                        point = LatLng(latitude, longitude)
                    )
                )
            }
        }

    }
}
