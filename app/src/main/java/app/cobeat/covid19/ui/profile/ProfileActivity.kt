package app.cobeat.covid19.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.ProfileActivityComponent
import app.cobeat.covid19.di.ProfileActivityComponentProvider
import app.cobeat.covid19.ui.BaseActivity
import app.cobeat.covid19.ui.questionnaire.QuestionnaireActivity
import app.cobeat.covid19.ui.safehouse.SafeHouseActivity
import app.cobeat.covid19.ui.safehouse.SafeHouseType
import app.cobeat.covid19.util.loadFragment
import app.cobeat.covid19.util.navigateForResult
import app.cobeat.covid19.util.navigateTo
import app.cobeat.covid19.util.showSuccessSnackBar
import kotlinx.android.synthetic.main.toolbar_back.*

private const val UPDATE_SAFE_HOUSE_REQUEST_CODE = 200

/**
 * Handles the Profile section.
 */
class ProfileActivity : BaseActivity(), ProfileNavigationHandler, ProfileActivityComponentProvider {

    private lateinit var profileActivityComponent: ProfileActivityComponent

    override fun getContentViewResource() = R.layout.activity_profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        profileActivityComponent =
            (application as ApplicationComponentProvider).getApplicationComponent()
                .profileActivityComponentBuilder()
                .profileNavigationHandler(this)
                .profileResourceProvider(buildProfileResourceProvider())
                .build()

        titleToolbar.text = resources.getString(R.string.profile)
        backButton.setOnClickListener { finish() }
        navigateToProfileFragment()
    }

    override fun navigateToProfileFragment() {
        loadFragment(ProfileFragment.newInstance(), addToBackStack = false)
    }

    override fun navigateToSafeHouseActivity() {
        navigateForResult<SafeHouseActivity>(UPDATE_SAFE_HOUSE_REQUEST_CODE) {
            putExtra(
                SafeHouseActivity.SAFE_HOUSE_TYPE, SafeHouseType.EDIT
            )
        }
    }

    override fun navigateToQuestionnaireActivity() {
        navigateTo<QuestionnaireActivity>()
    }

    override fun getProfileActivityComponent() = profileActivityComponent

    private fun buildProfileResourceProvider() =
        ProfileResourceProvider(
            saveProfileSuccessMessage = getString(R.string.profile_save_success_message)
        )

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UPDATE_SAFE_HOUSE_REQUEST_CODE
            && resultCode == Activity.RESULT_OK
        ) {
            showSuccessSnackBar(R.string.profile_address_update_success)
        }
    }
}
