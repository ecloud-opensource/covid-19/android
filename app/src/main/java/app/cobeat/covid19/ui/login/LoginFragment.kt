package app.cobeat.covid19.ui.login

import android.os.Bundle
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.text.HtmlCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import app.cobeat.covid19.R
import app.cobeat.covid19.di.LoginActivityComponentProvider
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.ui.EventObserver
import app.cobeat.covid19.util.hideLoadingView
import app.cobeat.covid19.util.showLoadingView
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject


class LoginFragment : BaseFragment() {

    @Inject
    lateinit var loginNavigationHandler: LoginNavigationHandler

    private val loginViewModel: ValidatePhoneViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as LoginActivityComponentProvider)
            .getLoginActivityComponentProvider()
            .inject(this)

        buttonContinue.setOnClickListener {
            showLoadingView()
            val phoneNumber = countryPickerRegisterNumber.fullNumberWithPlus
            loginViewModel.requestPhoneValidation(phoneNumber)
        }
        view?.apply {
            val phoneNumberEditText = findViewById<EditText>(R.id.phoneNumberEditText)

            countryPickerRegisterNumber.apply {
                registerCarrierNumberEditText(phoneNumberEditText)
                setDialogTypeFace(phoneNumberEditText.typeface)
                setTypeFace(phoneNumberEditText.typeface)
            }

            phoneNumberEditText.doAfterTextChanged {
                textInputLayout.error = null
            }

            loginViewModel.validatePhone.observe(viewLifecycleOwner, EventObserver {
                hideLoadingView()
                loginNavigationHandler.navigateToActivationCodeFragment(loginViewModel.selectedPhoneNumber)
            })

            val text = getString(R.string.accept_terms_text)
            val styledText: Spanned = HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_LEGACY)

            termsTextView.text = styledText
            termsTextView.setOnClickListener {
                loginNavigationHandler.navigateToTerms()
            }

            loginViewModel.errorBannerMessage.observe(viewLifecycleOwner, EventObserver {
                hideLoadingView()
                textInputLayout.error = it
            })

            countryPickerRegisterNumber.fullNumber = loginViewModel.selectedPhoneNumber

            loginViewModel.clearPhoneNumber.observe(viewLifecycleOwner, EventObserver {
                countryPickerRegisterNumber.fullNumber = ""
            })
        }
    }
}
