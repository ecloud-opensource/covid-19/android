package app.cobeat.covid19.ui.safehouse

import android.location.Location
import androidx.lifecycle.*
import app.cobeat.covid19.R
import app.cobeat.covid19.domain.SafeHouse
import app.cobeat.covid19.domain.UserRepository
import app.cobeat.covid19.ui.Event
import app.cobeat.covid19.ui.LocationHandler
import app.cobeat.covid19.ui.MapHandler
import app.cobeat.covid19.ui.PermissionHandler
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Success
import com.google.android.libraries.places.api.model.Place
import javax.inject.Inject

class SafeHouseViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val safeHouseResourceProvider: SafeHouseResourceProvider,
    private val locationHandler: LocationHandler,
    private val permissionHandler: PermissionHandler,
    private val placesHandler: PlacesHandler,
    private val mapHandler: MapHandler
) :
    ViewModel() {

    private val saveSafeHouse: MutableLiveData<SafeHouse> = MutableLiveData()

    init {
        locationHandler.locationListener = {
            when (it) {
                is Success -> {
                    _lastLocation.value = it.value
                    getCurrentPlace()
                }
                is Failure -> _locationUpdateError.value = Event(Unit)
            }
        }
    }

    private val _lastLocation = MutableLiveData<Location>()
    val lastLocation: LiveData<Location>
        get() = _lastLocation

    private val _locationUpdateError = MutableLiveData<Event<Unit>>()
    val locationUpdateError: LiveData<Event<Unit>>
        get() = _locationUpdateError

    private val _errorBannerMessage = MutableLiveData<Event<String>>()
    val errorBannerMessage: LiveData<Event<String>>
        get() = _errorBannerMessage

    /**
     * If the user has foreground location enabled this will fetch the last known location.
     * @return true if the user has foreground location and the fetch is started. False otherwise.
     */
    fun obtainLastLocation(): Boolean {
        val hasForegroundLocation = permissionHandler.hasForegroundLocation()
        if (hasForegroundLocation) {
            locationHandler.getLastLocationAndNotifyListener()
        }

        return hasForegroundLocation
    }

    private val _currentPlace = MutableLiveData<Event<SelectedPlace>>()
    val currentPlace: LiveData<Event<SelectedPlace>>
        get() = _currentPlace


    private fun getCurrentPlace() {
        placesHandler.findCurrentPlace {
            _currentPlace.value = Event(it)
        }
    }

    fun isValidPlace(selectedPlace: SelectedPlace?) =
        selectedPlace != null
                && selectedPlace.streetName.isNotBlank()
                && selectedPlace.streetNumber.isNotBlank()

    fun onNewPlaceSelected(place: Place) {
        place.latLng?.apply {
            mapHandler.clearMarkers()
            mapHandler.addMarker(
                this,
                safeHouseResourceProvider.quarantineMapPinTitle,
                R.mipmap.blue_status
            )
            mapHandler.moveCameraToLocation(latitude, longitude)
        }
        _currentPlace.value = Event(placesHandler.transformToSelectedPlace(place))
    }

    fun saveSafeHouse(safeHouse: SafeHouse) {
        saveSafeHouse.value = safeHouse
    }

    val safeHouseSaveSuccess: LiveData<Event<String>> =
        Transformations.switchMap(saveSafeHouse) {
            liveData {
                when (val result = userRepository.saveSafeHouse(it)) {
                    is Success -> emit(Event(safeHouseResourceProvider.saveSafeHouseSuccessMessage))
                    is Failure -> _errorBannerMessage.value = Event(result.error.message.toString())
                }
            }
        }
}
