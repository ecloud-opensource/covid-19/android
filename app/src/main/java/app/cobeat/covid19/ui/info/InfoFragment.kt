package app.cobeat.covid19.ui.info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import app.cobeat.covid19.R
import app.cobeat.covid19.di.InfoActivityComponentProvider
import app.cobeat.covid19.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_info.*
import javax.inject.Inject

class InfoFragment : BaseFragment() {

    @Inject
    lateinit var infoNavigationHandler: InfoNavigationHandler
    private val infoViewModel: InfoViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = InfoFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_info, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as InfoActivityComponentProvider)
            .getInfoActivityComponent()
            .inject(this)

        infoViewModel.errorBannerMessage.observe(viewLifecycleOwner, Observer {
            noLinksAvailableMessage.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        })

        infoViewModel.infoLinkList.observe(viewLifecycleOwner, Observer {

            recyclerView.apply {

                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(requireContext())

                adapter = InfoAdapter(it) {
                    infoNavigationHandler.navigateToLink(it.url)
                }
            }
        })

        infoViewModel.getInfoLinkList()
    }

}