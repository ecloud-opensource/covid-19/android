package app.cobeat.covid19.ui.web

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.cobeat.covid19.R
import app.cobeat.covid19.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_web_view.*

class WebFragment : BaseFragment(), WebActions {

    companion object {
        private const val TAG = "WebFragment"
        private const val ARG_BUNDLE_URL = "arg_bundle_url"
        private const val JAVASCRIPT_INTERFACE_NAME = "Mobile"

        @JvmStatic
        fun newInstance(url: String) = WebFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_BUNDLE_URL, url)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setUpWebCommunicationInterface()

        val url = arguments?.getString(ARG_BUNDLE_URL)
        val uri = Uri.parse(url)
            .buildUpon()
            .build()

        loadUri(uri)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_web_view, container, false)
    }

    @SuppressLint("JavascriptInterface", "SetJavaScriptEnabled")
    private fun setUpWebCommunicationInterface() {
        webView.settings.javaScriptEnabled = true
        val webAppInterfaceProvider = activity
                as? WebInterfaceProvider<*>

        webAppInterfaceProvider?.let {
            webView.addJavascriptInterface(
                it.getWebInterface(),
                JAVASCRIPT_INTERFACE_NAME
            )
        }
    }

    private fun loadUri(uri: Uri) {

        Log.d(TAG, "Loading: $uri")
        Log.d(TAG, "Path: ${uri.path}")
        Log.d(TAG, "User Token: ${uri.getQueryParameter("x-api-key")}")

        webView.loadUrl(uri.toString())
    }

    override fun canGoBack(): Boolean {
        return webView.canGoBack()
    }

    override fun goBack() {
        webView.goBack()
    }
}