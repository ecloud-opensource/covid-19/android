package app.cobeat.covid19.ui.contact

interface ContactNavigationHandler {
    fun navigateToContactFragment()

    fun navigateToHomeActivity()
}