package app.cobeat.covid19.ui.contact

data class ContactResourceProvider(
    val contactSuccessMessage: String
)