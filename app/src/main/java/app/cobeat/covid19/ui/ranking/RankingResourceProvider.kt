package app.cobeat.covid19.ui.ranking

data class RankingResourceProvider(
    val rankingLoadingErrorMessage: String
)