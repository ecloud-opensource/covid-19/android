package app.cobeat.covid19.ui.login

interface LoginNavigationHandler {

    fun navigateToActivationCodeFragment(phoneNumber: String)

    fun navigateToHomeActivity()

    fun navigateToTerms()

    fun navigateToLoginFragment()

    fun navigateBack()

    fun navigateToSafeHouseActivity()
}