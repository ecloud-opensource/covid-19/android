package app.cobeat.covid19.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import app.cobeat.covid19.R
import app.cobeat.covid19.di.HomeActivityComponentProvider
import app.cobeat.covid19.ui.BaseFragment
import app.cobeat.covid19.ui.MapHandler
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject


class HomeFragment : BaseFragment() {

    @Inject
    lateinit var homeNavigationHandler: HomeNavigationHandler

    @Inject
    lateinit var mapHandler: MapHandler

    private val homeViewModel: HomeViewModel
            by viewModels(
                factoryProducer = { viewModelFactory },
                ownerProducer = { requireActivity() })

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as HomeActivityComponentProvider)
            .getHomeActivityComponentProvider()
            .inject(this)

        setupViewElements()
    }

    private fun setupViewElements() {
        setupMap()
        mapHeaderClose.setOnClickListener { hideMapHeader() }
    }

    private fun setupMap() {
        (childFragmentManager.findFragmentById(R.id.home_map) as SupportMapFragment)
            .getMapAsync(mapHandler)
        homeViewModel.lastLocation.observe(viewLifecycleOwner, Observer {
            mapHandler.moveCameraToLocation(it)
        })
        mapHandler.onCameraMoveListener = {
            hideMapHeader()
        }
        mapHandler.onCameraIdleListener = {
            homeViewModel.getNearby(it)
        }
        homeViewModel.nearby.observe(viewLifecycleOwner, Observer { result ->
            result.markers.forEach {
                mapHandler.addMarker(
                    it.latLng,
                    getString(R.string.home_map_marker_status_message, it.status),
                    if (it.isQuarantined) R.mipmap.red_status else R.mipmap.blue_status
                )
            }

            result.headerNotification?.let {
                showMapHeader(it)
            }
        })
    }

    private fun showMapHeader(text: String) {
        mapHeaderText.text = text
        mapHeaderContainer.visibility = View.VISIBLE
    }

    private fun hideMapHeader() {
        mapHeaderContainer.visibility = View.GONE
    }
}
