package app.cobeat.covid19.ui.home

import com.google.android.gms.maps.model.LatLng

data class NearbyResultsUi(
    val headerNotification: String?,
    val footerNotifications: String?,
    val markers: List<UserMarkers>
) {
    data class UserMarkers(
        val isQuarantined: Boolean,
        val status: String,
        val latLng: LatLng
    )

}