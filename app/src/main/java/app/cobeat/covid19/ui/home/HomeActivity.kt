package app.cobeat.covid19.ui.home

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import app.cobeat.covid19.R
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.di.HomeActivityComponent
import app.cobeat.covid19.di.HomeActivityComponentProvider
import app.cobeat.covid19.domain.SettingsManager
import app.cobeat.covid19.network.tracking.LocationTrackingWorker
import app.cobeat.covid19.ui.*
import app.cobeat.covid19.ui.contact.ContactActivity
import app.cobeat.covid19.ui.info.InfoActivity
import app.cobeat.covid19.ui.login.LoginActivity
import app.cobeat.covid19.ui.profile.ProfileActivity
import app.cobeat.covid19.ui.questionnaire.QuestionnaireActivity
import app.cobeat.covid19.ui.ranking.RankingActivity
import app.cobeat.covid19.ui.web.WebActivity
import app.cobeat.covid19.util.hasBackgroundLocationPermission
import app.cobeat.covid19.util.loadFragment
import app.cobeat.covid19.util.navigateTo
import javax.inject.Inject

/**
 * Central Application Activity. This Activity handles the menu and the map. It's the activity that will be
 * displayed after the login section ([LoginActivity]). This activity will be displayed first if the user is already
 * logged in.
 */
class HomeActivity : BaseActivity(), HomeNavigationHandler, HomeActivityComponentProvider {

    private lateinit var homeActivityComponent: HomeActivityComponent
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var locationHandler: LocationHandler
    private lateinit var permissionHandler: PermissionHandler
    private lateinit var mapHandler: MapHandler

    @Inject
    protected lateinit var viewModelFactory: ViewModelFactory
    private val homeViewModel: HomeViewModel
            by viewModels(
                factoryProducer = { viewModelFactory })

    @Inject
    protected lateinit var settingsManager: SettingsManager

    override fun getContentViewResource() = R.layout.activity_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        locationHandler = LocationHandler(this)
        permissionHandler = PermissionHandler(this)
        mapHandler = MapHandler(this, locationHandler)

        homeActivityComponent =
            (application as ApplicationComponentProvider).getApplicationComponent()
                .homeActivityComponentBuilder()
                .homeNavigationHandler(this)
                .homeResourceProvider(buildHomeResourceProvider())
                .locationHandler(locationHandler)
                .permissionHandler(permissionHandler)
                .mapHandler(mapHandler)
                .build()

        homeActivityComponent.inject(this)

        setUpDrawerView()

        permissionHandler.askLocationPermissions()

        if (hasBackgroundLocationPermission(this)) {
            LocationTrackingWorker.startWorker(this)
        }

        loadFragment(
            fragment = HomeFragment.newInstance(),
            addToBackStack = false,
            container = R.id.main_container
        )
        loadFragment(
            fragment = HomeBottomFragment.newInstance(),
            addToBackStack = false,
            container = R.id.bottom_container
        )
    }

    override fun getHomeActivityComponentProvider() = homeActivityComponent

    override fun navigateToContactActivity() {
        navigateTo<ContactActivity>()
    }

    override fun navigateToQuestionnaire() {
        navigateTo<QuestionnaireActivity>()
    }

    override fun navigateToInfoActivity() {
        navigateTo<InfoActivity>()
    }

    override fun navigateToProfileActivity() {
        navigateTo<ProfileActivity>()
    }

    override fun navigateToRankingActivity() {
        navigateTo<RankingActivity>()
    }

    override fun openCityFeedUrl(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    override fun dialEmergencyNumber() {
        startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:107")))
    }

    override fun dialViolationReportNumber() {
        startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:144")))
    }

    private fun setUpDrawerView() {

        val toolbarHome = findViewById<View>(R.id.toolbarHome).findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbarHome)
        drawerLayout = findViewById(R.id.drawer_layout)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbarHome, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        drawerLayout.findViewById<View>(R.id.nav_profile).setOnClickListener {
            navigateToProfileActivity()
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        drawerLayout.findViewById<View>(R.id.nav_info).setOnClickListener {
            navigateToInfoActivity()
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        drawerLayout.findViewById<View>(R.id.nav_ranking).setOnClickListener {
            navigateToRankingActivity()
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        drawerLayout.findViewById<View>(R.id.nav_contact).setOnClickListener {
            navigateToContactActivity()
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        drawerLayout.findViewById<View>(R.id.nav_terms_and_conditions).setOnClickListener {
            navigateTo<WebActivity> {
                putExtra(WebActivity.ARG_URL, settingsManager.getTermsAndConditionsUri().toString())
                putExtra(WebActivity.ARG_TOOLBAR_TITLE, getString(R.string.terms_and_conditions))
            }
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        drawerLayout.findViewById<View>(R.id.close_drawer).setOnClickListener {
            drawerLayout.closeDrawer(GravityCompat.START)
        }

        val homeMenuHeaderTextView = drawerLayout.findViewById<TextView>(R.id.home_menu_header)

        homeViewModel.homeMenuHeader.observe(this, Observer {
            homeMenuHeaderTextView.text = it
        })

        homeViewModel.populateHomeMenuHeader()
    }

    private fun buildHomeResourceProvider() =
        HomeResourceProvider(
            getQuarantinedUser = getString(R.string.quarantined),
            getStatusOk = getString(R.string.status_ok),
            homeMenuHeader = getString(R.string.home_menu_header)
        )

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissionHandler.isForegroundLocationPermissionGranted(requestCode)) {
            mapHandler.displayMyLocation()
        }

        if (permissionHandler.isBackgroundLocationPermissionGranted(requestCode)) {
            LocationTrackingWorker.startWorker(this)
        }

        //TODO Handle the scenario where the user doesn't enable the location
    }
}
