package app.cobeat.covid19.ui

import android.app.Activity
import android.location.Location
import android.util.Log
import app.cobeat.covid19.util.Failure
import app.cobeat.covid19.util.Result
import app.cobeat.covid19.util.Success
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

/**
 * Obtains the location and communicates it to the subscriber [locationListener].
 * Wraps the interactions with [FusedLocationProviderClient].
 *
 * This class should be only referenced by the [Activity] or other classes that live along with the host [Activity].
 */
class LocationHandler(activity: Activity) {

    companion object {
        private const val TAG = "LocationHandler"
    }

    var locationListener: ((Result<Location>) -> Unit)? = null

    private val fusedLocationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(activity)

    fun getLastLocationAndNotifyListener() {
        fusedLocationClient.lastLocation.addOnCompleteListener {
            val location = it.result
            if (it.isSuccessful && location != null) {
                locationListener?.invoke(Success(location))
            } else {
                locationListener?.invoke(
                    Failure(
                        it.exception ?: IllegalStateException("Location not available")
                    )
                )
                Log.e(TAG, it.exception?.message ?: it.exception.toString())
            }
        }
    }

}