package app.cobeat.covid19.ui


interface LoadingViewHandler {

    fun showLoadingView()

    fun hideLoadingView()
}