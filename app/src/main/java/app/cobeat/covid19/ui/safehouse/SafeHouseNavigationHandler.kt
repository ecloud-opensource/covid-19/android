package app.cobeat.covid19.ui.safehouse

interface SafeHouseNavigationHandler {
    fun navigateToHomeActivity()

    fun navigateToProfileActivity()

    fun onSafeHouseSaved()
}

