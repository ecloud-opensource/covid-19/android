package app.cobeat.covid19.ui.web

import android.os.Bundle
import app.cobeat.covid19.R
import app.cobeat.covid19.ui.BaseActivity
import app.cobeat.covid19.util.loadFragment
import kotlinx.android.synthetic.main.toolbar_back.*

open class WebActivity : BaseActivity() {

    companion object {
        const val ARG_URL = "arg_url"
        const val ARG_TOOLBAR_TITLE = "arg_toolbar_title"
    }

    override fun getContentViewResource() = R.layout.activity_web

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        backButton?.setOnClickListener { onBackPressed() }

        intent.getStringExtra(ARG_TOOLBAR_TITLE)?.let {
            titleToolbar.text = it
        }
        intent.getStringExtra(ARG_URL)?.let {
            loadWeb(it)
        }
    }

    fun loadWeb(url: String) {
        loadFragment(WebFragment.newInstance(url), addToBackStack = false)
    }

    override fun onBackPressed() {

        val webActions = supportFragmentManager
            .findFragmentById(R.id.main_container) as WebActions
        if (webActions.canGoBack()) {
            webActions.goBack()
            return
        }

        super.onBackPressed()
    }
}