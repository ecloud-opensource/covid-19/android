package app.cobeat.covid19.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import app.cobeat.covid19.ui.LoadingViewHandler


fun Fragment.showSuccessSnackBar(stringResource: Int) {
    requireActivity().showSuccessSnackBar(stringResource)
}

fun Fragment.showSuccessSnackBar(message: String) {
    requireActivity().showSuccessSnackBar(message)
}

fun Fragment.showErrorSnackBar(stringResource: Int) {
    requireActivity().showErrorSnackBar(stringResource)
}

fun Fragment.showErrorSnackBar(message: String) {
    requireActivity().showErrorSnackBar(message)
}

fun Fragment.showErrorSnackBar(
    message: String,
    textAction: String,
    listener: View.OnClickListener
) {
    requireActivity().showErrorSnackBar(message, textAction, listener)
}

fun Fragment.showLoadingView() {
    val activity = requireActivity()
    if (activity is LoadingViewHandler) {
        activity.showLoadingView()
    }
}

fun Fragment.hideLoadingView() {
    val activity = requireActivity()
    if (activity is LoadingViewHandler) {
        activity.hideLoadingView()
    }
}

fun Fragment.hideKeyboard() {
    try {
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                InputMethodManager.SHOW_FORCED
            )
    } catch (e: Exception) {
        // Silently fail ... Nobody will notice shh ...
    }
}