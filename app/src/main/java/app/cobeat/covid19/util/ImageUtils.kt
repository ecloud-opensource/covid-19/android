package app.cobeat.covid19.util

import android.content.Context
import android.widget.ImageView
import app.cobeat.covid19.R
import com.bumptech.glide.Glide

class ImageUtils {
    companion object FontUtils {
        fun loadCenterCropImage(
            context: Context,
            sourceUrl: String,
            element: ImageView,
            placeholder: Int = R.drawable.splash
        ) {
            Glide.with(context)
                .load(sourceUrl)
                .centerCrop()
                .placeholder(placeholder)
                .into(element)
        }

        fun loadCenteredInsideImage(
            context: Context,
            sourceUrl: String,
            element: ImageView,
            placeholder: Int = R.drawable.splash
        ) {
            Glide.with(context)
                .load(sourceUrl)
                .centerInside()
                .placeholder(placeholder)
                .into(element)
        }
    }
}