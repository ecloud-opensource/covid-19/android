package app.cobeat.covid19.util

import android.content.Context
import android.graphics.Typeface
import android.widget.TextView
import java.util.*

class FontUtils {

    companion object FontUtils {
        private val fontCache = Hashtable<String, Typeface>()

        fun setCustomFont(
            textView: TextView?,
            context: Context,
            font: String?
        ) {
            if (font == null) {
                return
            }
            val tf = getTypeface(context, font)
            if (tf != null) {
                textView?.let {
                    it.typeface = tf
                }
            }
        }

        fun getTypeface(context: Context, name: String): Typeface? {
            var tf: Typeface? = fontCache.get(name)
            if (tf == null) {
                tf = try {
                    Typeface.createFromAsset(context.assets, name)
                } catch (e: Exception) {
                    return null
                }
                fontCache[name] = tf
            }
            return tf
        }
    }
}