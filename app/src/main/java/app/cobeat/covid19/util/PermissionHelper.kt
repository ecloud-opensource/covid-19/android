package app.cobeat.covid19.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat

fun hasForegroundLocationPermission(context: Context): Boolean {
    return checkPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION)
}

fun hasBackgroundLocationPermission(context: Context): Boolean {
    return checkPermissionGranted(context, Manifest.permission.ACCESS_COARSE_LOCATION)
            && checkPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION)
            && (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q
            || checkPermissionGranted(context, Manifest.permission.ACCESS_BACKGROUND_LOCATION))
}

fun checkPermissionGranted(context: Context, permission: String): Boolean {
    return (ContextCompat.checkSelfPermission(
        context,
        permission
    ) == PackageManager.PERMISSION_GRANTED)
}
