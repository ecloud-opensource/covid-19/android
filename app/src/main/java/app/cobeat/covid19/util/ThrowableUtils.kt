package app.cobeat.covid19.util

import android.util.Log
import app.cobeat.covid19.network.InternalException
import app.cobeat.covid19.network.ServerException
import java.util.*

val operationErrorMessage: String by lazy { if (Locale.getDefault().language == "es") "Fallo inesperado" else "Unexpected error" }

/**
 * Process the general exceptions.
 */
fun <T> processSupportedThrowable(t: Throwable): Failure<T> {
    Log.e("Error", t.message ?: t.toString())
    return when (t) {
        is ServerException, is InternalException -> Failure(t)
        else -> Failure(ServerException(operationErrorMessage))
    }
}