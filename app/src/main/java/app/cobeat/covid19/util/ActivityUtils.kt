package app.cobeat.covid19.util

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import app.cobeat.covid19.R
import com.pd.chocobar.ChocoBar

inline fun <reified T : Activity> Activity.navigateTo(
    finish: Boolean = false,
    noinline extrasProvider: (Intent.() -> Unit)? = null
) {
    val intent = Intent(this, T::class.java)
    extrasProvider?.let { intent.extrasProvider() }
    startActivity(intent)
    if (finish) {
        finish()
    }
}

inline fun <reified T : Activity> Activity.navigateForResult(
    requestCode: Int,
    noinline extrasProvider: (Intent.() -> Unit)? = null
) {
    val intent = Intent(this, T::class.java)
    extrasProvider?.let { intent.extrasProvider() }
    startActivityForResult(intent, requestCode)
}

/**
 * Loads a new [Fragment] in the main container [R.id.main_container].
 */
fun FragmentActivity.loadFragment(
    fragment: Fragment,
    addToBackStack: Boolean = true,
    container: Int = R.id.main_container
) {
    with(supportFragmentManager.beginTransaction()) {
        replace(container, fragment)
        if (addToBackStack) {
            addToBackStack(fragment::class.java.simpleName)
        }
        commit()
    }
}

fun FragmentActivity.navigateOneStepBack() {
    supportFragmentManager.popBackStack()
}

fun FragmentActivity.showSuccessSnackBar(stringResource: Int) {
    showSuccessSnackBar(getString(stringResource))
}

fun FragmentActivity.showSuccessSnackBar(message: String) {
    ChocoBar.builder().setActivity(this)
        .setText(message)
        .setTextColor(resources.getColor(R.color.colorTextLight))
        .setIcon(R.mipmap.check_on)
        .setBackgroundColor(resources.getColor(R.color.colorGreen))
        .setDuration(ChocoBar.LENGTH_SHORT)
        .setActionTextTypeface(FontUtils.getTypeface(this, "fonts/GothamRoundedMedium.ttf"))
        .build()
        .show()
}

fun FragmentActivity.showErrorSnackBar(stringResource: Int) {
    showErrorSnackBar(getString(stringResource))
}

fun FragmentActivity.showErrorSnackBar(message: String) {
    ChocoBar.builder().setActivity(this)
        .setText(message)
        .setTextColor(resources.getColor(R.color.colorTextLight))
        .setIcon(R.mipmap.cross)
        .setBackgroundColor(resources.getColor(R.color.colorRed))
        .setDuration(ChocoBar.LENGTH_SHORT)
        .setActionTextTypeface(FontUtils.getTypeface(this, "fonts/GothamRoundedMedium.ttf"))
        .build()
        .show()
}

fun FragmentActivity.showErrorSnackBar(
    message: String,
    textAction: String,
    listener: View.OnClickListener
) {
    ChocoBar.builder().setActivity(this)
        .setActionText(textAction)
        .setText(message)
        .setTextColor(resources.getColor(R.color.colorTextLight))
        .setIcon(R.mipmap.cross)
        .setBackgroundColor(resources.getColor(R.color.colorRed))
        .setDuration(ChocoBar.LENGTH_SHORT)
        .setActionTextTypeface(FontUtils.getTypeface(this, "fonts/GothamRoundedMedium.ttf"))
        .setActionClickListener(listener)
        .build()
        .show()
}