package app.cobeat.covid19.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import app.cobeat.covid19.R

object MailHelper {

    fun sendEmailToSupport(context: Context) {
        context.let {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", it.resources.getString(R.string.support_email), null
                )
            )
            it.startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }
    }

    fun getEmailIntentToSupport(context: Context): Intent {
        context.let {
            return Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", it.resources.getString(R.string.support_email), null
                )
            )
        }
    }
}