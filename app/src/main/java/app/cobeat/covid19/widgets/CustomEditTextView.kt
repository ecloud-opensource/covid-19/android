package app.cobeat.covid19.widgets

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import app.cobeat.covid19.R
import app.cobeat.covid19.util.FontUtils

class CustomEditTextView : AppCompatEditText {

    constructor(context: Context?) : super(context) {
        setCustomFont(this, context, null)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        setCustomFont(this, context, attrs)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(
        context, attrs, defStyleAttr
    ) {
        setCustomFont(this, context, attrs)
    }

    private fun setCustomFont(
        editText: AppCompatEditText?,
        context: Context?,
        attrs: AttributeSet?
    ) {
        context?.let {
            val a = context.obtainStyledAttributes(attrs, R.styleable.CustomFont)
            val font = a.getString(R.styleable.CustomFont_fontvalue)
            FontUtils.setCustomFont(editText, context, font)
            a.recycle()
        }
    }

    override fun setError(
        error: CharSequence?,
        icon: Drawable?
    ) {
        setCompoundDrawables(null, null, icon, null)
    }
}