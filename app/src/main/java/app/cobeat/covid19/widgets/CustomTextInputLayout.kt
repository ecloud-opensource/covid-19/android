package app.cobeat.covid19.widgets

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import app.cobeat.covid19.R
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.edit_text_error_message.view.*

class CustomTextInputLayout : TextInputLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr)

    override fun onFinishInflate() {
        super.onFinishInflate()
        LayoutInflater.from(context).inflate(R.layout.edit_text_error_message, this, true)
        setOnClickListener {
            error = null
        }
    }

    override fun setError(errorText: CharSequence?) {
        editText?.background = if (errorText != null) {
            errorMessage.text = errorText
            inputErrorContainer.visibility = View.VISIBLE
            backgroundError
        } else {
            inputErrorContainer.visibility = View.GONE
            defaultBackground
        }
    }

    private val backgroundError: Drawable by lazy {
        resources.getDrawable(
            R.drawable.background_gray_light_rounded_red_border,
            resources.newTheme()
        )
    }

    private val defaultBackground: Drawable by lazy {
        resources.getDrawable(
            R.drawable.background_gray_light_rounded,
            resources.newTheme()
        )
    }

}