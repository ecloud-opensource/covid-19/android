package app.cobeat.covid19.widgets

import android.animation.Animator
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import app.cobeat.covid19.R
import kotlinx.android.synthetic.main.view_input_text_custom.view.*


class CustomInputTextView : RelativeLayout {

    constructor(context: Context?) : super(context) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        setUp()
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        setUp()
    }


    private fun setUp() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        addView(inflater.inflate(R.layout.view_input_text_custom, this, false))
    }

    fun setTitleText(titleText: String?) {
        titleText?.let {
            title.text = it
        }
    }

    fun setInputText(inputText: String?) {
        inputText?.let {
            input.setText(it)
        }
    }

    fun setInputTye(type: Int) {
        input.inputType = type
    }

    fun getInputText(): String {
        return input.text.toString()
    }

    fun setTextColor(color: Int) {
        input.setTextColor(color)
    }

    fun setError(error: Boolean): Boolean {
        lineFooter.setBackgroundColor(context.resources.getColor(if (error) R.color.colorRed else R.color.colorTextGray))
        if (error) {
            errorMessageLabel?.animate()?.alpha(1.0f)
                ?.setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(p0: Animator?) {

                    }

                    override fun onAnimationEnd(p0: Animator?) {
                        errorMessageLabel?.animate()?.alpha(0.0f)?.duration = 4000
                    }

                    override fun onAnimationCancel(p0: Animator?) {

                    }

                    override fun onAnimationStart(p0: Animator?) {

                    }

                })?.duration = 2000
        }

        return !error
    }

    fun setHint(hint: String) {
        input.hint = hint
    }

    fun setHintColor(idColor: Int) {
        input.setHintTextColor(context.resources.getColor(idColor))
    }
}