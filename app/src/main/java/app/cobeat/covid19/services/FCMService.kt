package app.cobeat.covid19.services

import android.util.Log
import app.cobeat.covid19.di.ApplicationComponentProvider
import app.cobeat.covid19.network.CobeatApi
import app.cobeat.covid19.network.ServicesFactory
import app.cobeat.covid19.network.UpdateUserRequestDto
import app.cobeat.covid19.storage.SecureStorage
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import javax.inject.Inject

class FCMService : FirebaseMessagingService() {

    companion object {
        private const val TAG = "FCMService"
    }

    @Inject
    lateinit var secureStorage: SecureStorage

    override fun onCreate() {
        super.onCreate()

        (application as ApplicationComponentProvider)
            .getApplicationComponent()
            .fcmServiceComponentBuilder()
            .build()
            .inject(this)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "onNewToken($token)")
        sendRegistrationToServer(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        logMessageReceived(remoteMessage)
        remoteMessage.notification?.let {
            notifyMessage(
                remoteMessage.notification?.title ?: "",
                remoteMessage.notification?.body ?: ""
            )
        }
    }

    private fun notifyMessage(
        title: String,
        body: String
    ) {

        NotificationWrapper(this)
            .createBuilder(title, body)
            .notifyInfo()
    }

    private fun sendRegistrationToServer(token: String) {
        if (!secureStorage.isRegistered()) return

        Log.d(TAG, "sendRegistrationTokenToServer($token)")
        val serviceFactory = ServicesFactory(baseContext)
        val api = serviceFactory.buildLocationTrackingService(CobeatApi::class.java)
        try {
            val result = api.putUserMe(
                UpdateUserRequestDto(
                    firebaseToken = token
                )
            ).execute().body()
            Log.d(TAG, "PUT /user/me: ${result?.toString()}")
        } catch (t: Throwable) {
            Log.e(TAG, "Error on update fcm token : $t")
        }
    }

    private fun logMessageReceived(remoteMessage: RemoteMessage) {
        val message = "onMessageReceived(" +
                "\nid:${remoteMessage.messageId}" +
                "\ntitle:${remoteMessage.notification?.title}" +
                "\nmessage:${remoteMessage.notification?.body}" +
                "\n)"
        Log.d(TAG, message)
    }


    fun loadFirebaseToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new Instance ID token
                val token = task.result?.token
                Log.d(TAG, "Firebase token: $token")
            })
    }
}