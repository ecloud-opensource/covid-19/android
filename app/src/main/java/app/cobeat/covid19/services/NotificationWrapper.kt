package app.cobeat.covid19.services

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import app.cobeat.covid19.R
import app.cobeat.covid19.ui.splash.SplashActivity

private const val CHANNEL_ID = "covid19_channel_id"

/**
 *Implementation details in https://developer.android.com/training/notify-user/build-notification
 */
class NotificationWrapper(
    val context: Context
) {

    var builder: NotificationCompat.Builder? = null

    /**
     * Create a default Notification Builder
     */
    fun createBuilder(title: String, body: String): NotificationWrapper {

        val intent = Intent(context, SplashActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

        builder = NotificationCompat.Builder(
            context,
            CHANNEL_ID
        )
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
            .setContentTitle(title)
            .setContentText(body)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(body)
            )
        return this
    }

    fun setAction(pendingIntent: PendingIntent): NotificationWrapper {
        builder?.setContentIntent(pendingIntent)
        return this
    }

    fun getNotificationBuilder(): NotificationCompat.Builder? {
        return builder
    }

    fun notifyInfo() {

        builder?.build()?.let { notification ->
            NotificationManagerCompat.from(context).apply {
                notify(0, notification)
            }
        }

    }

}